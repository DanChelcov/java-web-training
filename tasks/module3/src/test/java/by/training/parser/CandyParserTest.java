package by.training.parser;

import by.training.commands.CandySaxParser;
import by.training.commands.CommandException;
import by.training.commands.DomCandyParser;
import by.training.entity.Candy;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import java.util.List;

import static org.junit.Assert.*;

public class CandyParserTest {
    Logger LOGGER = LogManager.getLogger(this.getClass());
    String xmlPath = "/Candies.xml";
    DomCandyParser domCandyParser;
    CandySaxParser candySaxParser;

    @Before
    public void init(){
        domCandyParser = new DomCandyParser();
        candySaxParser = new CandySaxParser();
    }

    @Test
    public void parseWithDOM() throws CommandException {
        LOGGER.log(Level.INFO, "--------------START DOM_PARSER TEST----------");
        List<Candy> list = domCandyParser.build(getClass().getResource(xmlPath).getPath());
        assertTrue(list != null);
        assertEquals(15, list.size());
        for(Candy candy: list){
            assertTrue(candy != null);
        }
        System.out.println(list.get(10).toString());

        LOGGER.log(Level.INFO, "--------------DOM_PARSER TEST FINISH----------");

    }
    @Test
    public void parseWithSAX() throws CommandException {
        LOGGER.log(Level.INFO, "--------------START DOM_PARSER TEST----------");
        List<Candy> list = candySaxParser.build(getClass().getResource(xmlPath).getPath());
        assertTrue(list != null);
        assertEquals(15, list.size());
        for(Candy candy: list){
            assertTrue(candy != null);
        }
        System.out.println(list.get(6).toString());
        LOGGER.log(Level.INFO, "--------------DOM_PARSER TEST FINISH----------");

    }



}