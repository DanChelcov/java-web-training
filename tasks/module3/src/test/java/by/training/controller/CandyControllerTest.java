package by.training.controller;

import by.training.commands.CandySaxParser;
import by.training.entity.Candy;
import by.training.commands.Command;
import by.training.commands.DomCandyParser;
import by.training.repository.CandyRepository;
import by.training.service.CandyService;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import java.util.List;

import static org.junit.Assert.*;

public class CandyControllerTest {
    CandyRepository candyRepository;
    CandyService candyService;
    CandyController candyController;
    Logger LOGGER = LogManager.getLogger(this.getClass());
    String pathToXML = this.getClass().getResource("/Candies.xml").getPath();
    CandyCommandsFactory candyCandyCommandsFactory;
    Command candyDOMCommand;
    Command candySAXCommand;


    @Before
    public void init() {
        candyRepository = new CandyRepository();
        candyService = new CandyService(candyRepository);
        candyCandyCommandsFactory = new CandyCommandsFactory();
        candyDOMCommand = new DomCandyParser();
        candySAXCommand = new CandySaxParser();
        candyCandyCommandsFactory.putCommand(TypeOfBuild.DOM, candyDOMCommand);
        candyCandyCommandsFactory.putCommand(TypeOfBuild.SAX, candySAXCommand);
        candyController = new CandyController(candyService,candyCandyCommandsFactory);
    }

    @Test
    public void testReadFromXMLUsingDOM()  {
        LOGGER.log(Level.INFO, "------------START READ_FROM_XML WITH DOM TEST------------");
        boolean res = candyController.readInfoFromXML(pathToXML, TypeOfBuild.DOM);
        List<Candy> list = candyRepository.getListOfItems();
        assertTrue(list != null);
        assertEquals(15, list.size());
        for(Candy candy: list){
            assertTrue(candy != null);
        }
        System.out.println(list.get(7));
        LOGGER.log(Level.INFO,"---------- READ_FROM_XML TEST FINISH------------");

    }
    @Test
    public void testReadFromXMLUsingSAX()  {
        LOGGER.log(Level.INFO, "------------START READ_FROM_XML WITH SAX TEST------------");
        boolean res = candyController.readInfoFromXML(pathToXML, TypeOfBuild.SAX);
        assertTrue(res);
        List<Candy> list = candyRepository.getListOfItems();
        assertEquals(15, list.size());
        for(Candy candy: list){
            assertTrue(candy != null);
        }
        System.out.println(list.get(7));
        LOGGER.log(Level.INFO,"---------- READ_FROM_XML TEST FINISH------------");

    }
}