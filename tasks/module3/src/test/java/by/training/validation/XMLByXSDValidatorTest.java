package by.training.validation;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class XMLByXSDValidatorTest {
    Logger LOGGER = LogManager.getLogger(this.getClass());
    XMLByXSDValidator validator;
    String xmlPath = "/Candies.xml";
    String xdsPath = "/Candies.xsd";
    String inValidXmlPath = "/inValidCandies.xml";

    @Before
    public void init(){
        validator = new XMLByXSDValidator(getClass().getResource(xdsPath).getPath());
    }

    @Test
    public void testValidXML(){
        LOGGER.log(Level.INFO, "---------TEST VALID XML---------");
        ValidationResult validationResult = validator.getValidationResult(getClass().getResource(xmlPath).getPath());
        assertTrue(validationResult.isValid());
        LOGGER.log(Level.INFO, "test is successful");
    }
    @Test
    public void testInValidXML(){
        LOGGER.log(Level.INFO, "---------TEST INVALID XML---------");
        ValidationResult validationResult = validator.getValidationResult(getClass().getResource(inValidXmlPath).getPath());
        assertFalse(validationResult.isValid());
        LOGGER.log(Level.INFO, validationResult.getAllVales());
        LOGGER.log(Level.INFO, "test is successful");
    }
}