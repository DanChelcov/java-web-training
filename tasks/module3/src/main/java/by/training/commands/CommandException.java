package by.training.commands;

public class CommandException extends Exception {
    public Exception exception;

    public CommandException() {

    }

    public CommandException(String message) {
        super(message);
    }

    public CommandException(String message, Exception exception) {
        super(message);
        this.exception = exception;
    }
}
