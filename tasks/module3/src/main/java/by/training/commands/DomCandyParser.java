package by.training.commands;

import by.training.entity.*;
import by.training.util.FromStringTo;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DomCandyParser implements Command<Candy> {
    DocumentBuilderFactory builderFactory;

    public DomCandyParser() {
        builderFactory = DocumentBuilderFactory.newInstance();
    }

    @Override
    public List<Candy> build(String parse) throws CommandException {
        DocumentBuilder builder = null;
        try {
            builder = builderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new CandyDomParserException("cant create document builder from document builder factory", e);
        }
        Document document = null;
        try {
            document = builder.parse(parse);
        } catch (SAXException e) {
            throw new CandyDomParserException("problem with XML syntax ", e);
        } catch (IOException e) {
            throw new CandyDomParserException("problem with file", e);
        }
        List<Candy> candyList = new ArrayList<>();
        Node rootNode = document.getDocumentElement();
        NodeList nodeList =cleanTextSpace(rootNode).getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            try {
                Node node = nodeList.item(i);

                Candy candy = null;
                switch (KindOfCandy.valueOf(node.getNodeName().toUpperCase())) {
                    case COMFIT:
                        candy = new Comfit();
                        break;
                    case COOKIE:
                        candy = new Cookie();
                        break;
                    case LOLLIPOP:
                        candy = new Lollipop();
                        break;
                }
                if (candy == null) {
                    continue;
                }
                candy.setId(FromStringTo.toLong(node.getAttributes().getNamedItem("id").getNodeValue()));
                NodeList childNotes = cleanTextSpace(node).getChildNodes();
                for (int k = 0; k < childNotes.getLength(); k++) {
                    Node cNode = childNotes.item(k);
                    switch (cNode.getNodeName()) {
                        case "Name":
                            candy.setName(cNode.getTextContent());
                            break;
                        case "Energy":
                            candy.setEnergy(FromStringTo.toInt(cNode.getTextContent()));
                            break;
                        case "Type":
                            Type type = Type.valueOf(cNode.getAttributes().getNamedItem("type").getNodeValue().toUpperCase());
                            type.setWithFilling(FromStringTo.toBoolean(cNode.getTextContent()));
                            candy.setType(type);
                            break;
                        case "Ingredients":
                            NodeList ingredients = cleanTextSpace(cNode).getChildNodes();
                            for (int j = 0; j < ingredients.getLength(); j++) {
                                Node ingredientNode = ingredients.item(j);
                                String qty = "";
                                String item = "";
                                NodeList list = cleanTextSpace(ingredientNode).getChildNodes();
                                for (int t = 0; t < list.getLength(); t++) {
                                    Node node1 = list.item(t);
                                    switch (node1.getNodeName()) {
                                        case "Qty":
                                            qty = node1.getTextContent() + " " + node1.getAttributes().getNamedItem("unit").getNodeValue();
                                            break;
                                        case "Item":
                                            item = node1.getTextContent();
                                    }
                                }
                                candy.setIngredient(item + ":" + qty);


                            }
                            break;
                        case "Value":
                            Value value = new Value();
                            NodeList list = cleanTextSpace(cNode).getChildNodes();
                            for (int t = 0; t < list.getLength(); t++) {
                                Node node1 = list.item(t);
                                if (node1 instanceof Element) {
                                    switch (node1.getNodeName()) {
                                        case "Protein":
                                            value.setProtein(FromStringTo.toInt(node1.getTextContent()));
                                            break;
                                        case "Fat":
                                            value.setFat(FromStringTo.toInt(node1.getTextContent()));
                                            break;
                                        case "Carbohydrate":
                                            value.setCarbohydrate(FromStringTo.toInt(node1.getTextContent()));
                                            break;
                                    }
                                }
                            }
                            candy.setValue(value);
                            break;
                        case "PlaceOfProduction":
                            candy.setPlaceOfProduction(cNode.getTextContent());
                    }

                }
                candyList.add(candy);
            }catch (IllegalArgumentException e){
                throw  new CommandException("Incorrect data in xml: " + e.getMessage() , e);
            }

        }
        return candyList;
    }

    private static Node cleanTextSpace(Node node){
        NodeList nodeList = node.getChildNodes();
        for(int i = 0; i < nodeList.getLength(); i++){
            if(nodeList.item(i).getNodeName() == "#text"){
                node.removeChild(nodeList.item(i));
            }
        }
        return node;
    }
}
