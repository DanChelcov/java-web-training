package by.training.commands;

import by.training.entity.*;
import by.training.util.FromStringTo;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.*;

public class CandySaxParser implements Command<Candy> {
    private static final Logger LOGGER = LogManager.getLogger(CandySaxParser.class);
    private SAXParserFactory saxParserFactory;
    private SAXParser saxParser;
    private CandySAXHandler candySAXHandler;

    public CandySaxParser() {
        saxParserFactory = SAXParserFactory.newInstance();
        try {
            saxParser = saxParserFactory.newSAXParser();
        } catch (ParserConfigurationException e) {
            LOGGER.log(Level.ERROR, "problems with parser conf", e);
        } catch (org.xml.sax.SAXException e) {
            LOGGER.log(Level.ERROR, "SAX exception", e);
        }
        candySAXHandler = new CandySAXHandler();
    }

    @Override
    public List<Candy> build(String parse) throws CommandException {

        try {
            saxParser.parse(parse, candySAXHandler);
        } catch (SAXException e) {
            throw new CandySaxParserException("problem with XML syntax " + e);
        } catch (IOException e) {
            throw new CandySaxParserException("problem with file" + e);
        }
        return candySAXHandler.candyList;
    }


    private static class CandySAXHandler extends DefaultHandler {
        private Candy candy = null;
        List<Candy> candyList;
        private String ingredientMeasure = null;
        EnumSet<CandyEnum> enumSet = EnumSet.range(CandyEnum.NAME, CandyEnum.PLACEOFPRODUCTION);
        CandyEnum candyEnum = null;

        public CandySAXHandler() {
            candyList = new ArrayList<>();
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if ("Comfit".equals(qName)) {
                candy = new Comfit(FromStringTo.toLong(attributes.getValue("id")));
            }
            if ("Cookie".equals(qName)) {
                candy = new Cookie(FromStringTo.toLong(attributes.getValue("id")));
            }
            if ("Lollipop".equals(qName)) {
                candy = new Lollipop(FromStringTo.toLong(attributes.getValue("id")));
            }
            try {
                candyEnum = CandyEnum.valueOf(qName.toUpperCase());
            } catch (Exception e) {
                candyEnum = null;
            }
            if ("Type".equals(qName)) {
                try {
                    candy.setType(Type.valueOf(attributes.getValue("type").toUpperCase()));
                } catch (Exception e) {
                    candy.setType(Type.DEFAULT);
                }
            }
            if ("Qty".equals(qName)) {
                ingredientMeasure = attributes.getValue("unit");
            }
            if ("Value".equals(qName)) {
                candy.setValue(new Value());
            }

        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if ("Ingredient".equals(qName) && candyEnum != null) {
                candy.setIngredient(candyEnum.ingredientInfo + " " + ingredientMeasure);
                candyEnum.ingredientInfo = null;
            }
            if (candyEnum != null && enumSet.contains(candyEnum)) {
                candyEnum = null;
            }
            if ("Comfit".equals(qName) || "Cookie".equals(qName) || "Lollipop".equals(qName)) {
                candyList.add(candy);
                candyEnum = null;
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String value = new String(ch, start, length).trim();
            if (candyEnum != null && !value.equals("")) {
                candyEnum.setValue(candy, value);
            }
        }


    }

    private enum CandyEnum {
        NAME {
            public void setValue(Candy candy, String value) {
                candy.setName(value);
            }
        },
        ENERGY {
            public void setValue(Candy candy, String value) {
                candy.setEnergy(FromStringTo.toInt(value));
            }
        },
        FILLING {
            public void setValue(Candy candy, String value) {
                candy.getType().setWithFilling(FromStringTo.toBoolean(value));
            }
        },
        PROTEIN {
            public void setValue(Candy candy, String value) {
                candy.getValue().setProtein(FromStringTo.toInt(value));
            }
        },
        FAT {
            public void setValue(Candy candy, String value) {
                candy.getValue().setFat(FromStringTo.toInt(value));
            }
        },
        CARBOHYDRATE {
            public void setValue(Candy candy, String value) {
                candy.getValue().setCarbohydrate(FromStringTo.toInt(value));
            }
        },
        PLACEOFPRODUCTION {
            public void setValue(Candy candy, String value) {
                candy.setPlaceOfProduction(value);
            }
        },

        QTY {
            public void setValue(Candy candy, String value) {
                setIngredientInfo(value);
            }
        },
        ITEM {
            public void setValue(Candy candy, String value) {
                setIngredientInfo(value + ":" + CandyEnum.QTY.getIngredientInfo());
            }
        };
        private String ingredientInfo;

        public void setIngredientInfo(String string) {
            ingredientInfo = string;
        }

        public String getIngredientInfo() {
            return ingredientInfo;
        }

        public void setValue(Candy candy, String value) {
        }
    }


}
