package by.training.commands;

public class CandyDomParserException extends CommandException {
    public CandyDomParserException() {
    }

    public CandyDomParserException(String message) {
        super(message);
    }

    public CandyDomParserException(String message, Exception exception) {
        super(message, exception);
    }
}
