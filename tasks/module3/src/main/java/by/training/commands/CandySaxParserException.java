package by.training.commands;

public class CandySaxParserException extends CommandException {
    public CandySaxParserException() {
    }

    public CandySaxParserException(String message) {
        super(message);
    }

    public CandySaxParserException(String message, Exception exception) {
        super(message, exception);
    }
}
