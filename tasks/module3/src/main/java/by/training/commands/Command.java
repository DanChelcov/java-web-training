package by.training.commands;

import java.util.List;

public interface Command<T> {
    List<T> build(String parse)throws  CommandException;

}
