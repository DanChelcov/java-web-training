package by.training.entity;

import java.util.Objects;

public class Value {
    private int protein;
    private int fat;
    private int carbohydrate;

    public Value() {
    }

    public Value(int protein, int fat, int carbohydrate) {
        this.protein = protein;
        this.fat = fat;
        this.carbohydrate = carbohydrate;
    }

    public int getProtein() {
        return protein;
    }

    public void setProtein(int protein) {
        this.protein = protein;
    }

    public int getFat() {
        return fat;
    }

    public void setFat(int fat) {
        this.fat = fat;
    }

    public int getCarbohydrate() {
        return carbohydrate;
    }

    public void setCarbohydrate(int carbohydrate) {
        this.carbohydrate = carbohydrate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Value value = (Value) o;
        return protein == value.protein &&
                fat == value.fat &&
                carbohydrate == value.carbohydrate;
    }

    @Override
    public int hashCode() {
        return Objects.hash(protein, fat, carbohydrate);
    }

    @Override
    public String toString() {
        return "Value{" +
                "protein = " + protein +
                ", fat = " + fat +
                ", carbohydrate = " + carbohydrate +
                '}';
    }
}
