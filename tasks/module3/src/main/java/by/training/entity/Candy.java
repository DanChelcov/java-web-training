package by.training.entity;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class Candy {
    protected KindOfCandy kindOfCandy;
    private long id;
    private String name;
    private int energy;
    private Type type;
    private List<String> ingredients;
    private Value value;
    private String placeOfProduction;

    public Candy() {
        ingredients = new ArrayList<>();
        type = Type.DEFAULT;
        value = new Value();

    }

    public Candy(long id) {
        ingredients = new ArrayList<>();
        type = Type.DEFAULT;
        value = new Value();
        this.id = id;
    }

    public KindOfCandy getKindOfCandy() {
        return kindOfCandy;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public List<String> getIngredients() {
        return new ArrayList<>(ingredients);
    }

    public void setIngredients(List<String> ingredients) {
        this.ingredients = ingredients;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    public String getPlaceOfProduction() {
        return placeOfProduction;
    }

    public void setPlaceOfProduction(String placeOfProduction) {
        this.placeOfProduction = placeOfProduction;
    }
    public void setIngredient(String ingredient){
        ingredients.add(ingredient);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Candy candy = (Candy) o;
        return id == candy.id &&
                energy == candy.energy &&
                type.equals(candy.type) &&
                kindOfCandy.equals(candy.kindOfCandy) &&
                name.equals(candy.name) &&
                Objects.equals(ingredients, candy.ingredients) &&
                value.equals(candy.value) &&
                placeOfProduction.equals(candy.placeOfProduction);
    }

    @Override
    public int hashCode() {
        return Objects.hash(kindOfCandy, id, name, energy, type, ingredients, value);
    }

    @Override
    public String toString() {
        return "Candy {" +
                "kindOfCandy = " + kindOfCandy +
                ", id = " + id +
                ", name = '" + name + '\'' +
                ", energy = " + energy +
                ", type = " + type +
                ", ingredients = " + ingredients +
                ", value = " + value +
                ", placeOfProduction = " + placeOfProduction +
                '}';
    }


}
