package by.training.entity;

public class Lollipop extends Candy {
    public Lollipop() {
        kindOfCandy = KindOfCandy.LOLLIPOP;
    }

    public Lollipop(long id) {
        super(id);
        kindOfCandy = KindOfCandy.LOLLIPOP;
    }

    @Override
    public String toString() {
        return super.toString();
    }


}
