package by.training.entity;

public enum Type {
    DEFAULT,
    CARAMEL,
    CHOCOLATE;
    private boolean withFilling;

    public boolean getWithFilling() {
        return withFilling;
    }

    public void setWithFilling(boolean withFilling) {
        this.withFilling = withFilling;
    }



}
