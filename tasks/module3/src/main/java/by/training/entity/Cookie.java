package by.training.entity;

public class Cookie extends Candy {
    public Cookie() {
        kindOfCandy = KindOfCandy.COOKIE;
    }

    public Cookie(long id) {
        super(id);
        kindOfCandy = KindOfCandy.COOKIE;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
