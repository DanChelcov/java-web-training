package by.training.entity;


public enum KindOfCandy {
    COMFIT,
    COOKIE,
    LOLLIPOP
}
