package by.training.entity;

public class Comfit extends Candy {
    public Comfit() {
        kindOfCandy = KindOfCandy.COMFIT;
    }

    public Comfit(long id) {
        super(id);
        kindOfCandy = KindOfCandy.COMFIT;
    }

    @Override
    public String toString() {
        return super.toString();
    }

}
