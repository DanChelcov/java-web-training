package by.training.service;

import by.training.entity.Candy;
import by.training.repository.IncorrectIdException;
import by.training.repository.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class CandyService implements Service<Candy> {
    private final Repository<Candy> candyRepository;

    public CandyService(Repository<Candy> candyRepository) {
        this.candyRepository = candyRepository;
    }

    @Override
    public long saveItem(Candy item) {
        return candyRepository.add(item);
    }

    @Override
    public void saveListOfItems(List<Candy> listItems) {
        listItems.stream().filter(Objects::nonNull).forEach(candyRepository::add);
    }

    @Override
    public boolean deleteById(long id) {
       return candyRepository.delete(id);
    }

    @Override
    public Candy readItemById(long id) throws ServiceException {
        Candy candy = null;
        try {
            candy = candyRepository.read(id);
        } catch (IncorrectIdException e){
            throw new ServiceException("cant read Candy by id", e);
        }
        return candy;
    }

    @Override
    public List<Candy> getSortedItems(Comparator<Candy> comparator) throws ServiceException {
        if (comparator == null) throw new ServiceException("cant sort candies : comparator is null");
        List<Candy> candyList = new ArrayList<>(candyRepository.getListOfItems());
        candyList.sort(comparator);
        return candyList;
    }

    @Override
    public List<Candy> getAllItems() {
       return candyRepository.getListOfItems();

    }
}
