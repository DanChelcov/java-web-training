package by.training.service;

public class ServiceException extends Exception {
    public Exception exception;

    public ServiceException() {

    }

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Exception e) {
        super(message);
        this.exception = e;
    }
}
