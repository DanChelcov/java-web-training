package by.training.service;

import java.util.Comparator;
import java.util.List;

public interface Service<T> {
    long saveItem(T item);

    void saveListOfItems(List<T> listItems);

    boolean deleteById(long id);

    T readItemById(long id) throws ServiceException;

    List<T> getSortedItems(Comparator<T> comparator) throws ServiceException;

    List<T> getAllItems();
}
