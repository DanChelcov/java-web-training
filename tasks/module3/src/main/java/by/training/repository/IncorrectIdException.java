package by.training.repository;

public class IncorrectIdException extends Exception {
    public Exception exception;
    public long incorrectId;

    public IncorrectIdException() {

    }

    public IncorrectIdException(String message, long incorrectId) {
        super(message);
        this.incorrectId = incorrectId;
    }

    public IncorrectIdException(Exception exception, long incorrectId) {
        this.exception = exception;
        this.incorrectId = incorrectId;
    }
}
