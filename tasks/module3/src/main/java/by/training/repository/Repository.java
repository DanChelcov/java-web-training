package by.training.repository;

import java.util.List;

public interface Repository<T> {
    long add(T item);

    boolean delete(long id);

    T read(long id) throws IncorrectIdException;

    List<T> getListOfItems();

    List<T> findBySpec(Specification<T> spec);
}
