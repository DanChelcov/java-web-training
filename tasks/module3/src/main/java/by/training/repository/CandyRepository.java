package by.training.repository;

import by.training.entity.Candy;

import java.util.ArrayList;
import java.util.List;

public class CandyRepository implements Repository<Candy> {
    private List<Candy> candyList;

    public CandyRepository() {
        candyList = new ArrayList<>();
    }

    @Override
    public long add(Candy item) {
        candyList.add(item);
        return item.getId();
    }

    @Override
    public boolean delete(long id) {
        if (candyList.stream().anyMatch(candy -> candy.getId() == id)) {
            for (Candy candy : candyList) {
                if (candy.getId() == id) {
                    candyList.remove(candy);
                    return true;
                }
            }
        }
        return false;
    }


    @Override
    public Candy read(long id) throws IncorrectIdException {
        List<Candy> list = new ArrayList<>();
        candyList.stream().filter(candy -> candy.getId() == id).forEach(list::add);
        if (list.isEmpty()) {
            throw new IncorrectIdException("This id is not exist", id);
        } else {
            return list.get(0);
        }
    }

    @Override
    public List<Candy> getListOfItems() {
        return new ArrayList<>(candyList);
    }


    @Override
    public List<Candy> findBySpec(Specification<Candy> spec) {
        List<Candy> listOfValidCandy = new ArrayList<>();
        candyList.stream().filter(spec::isSatisfiedBy).forEach(listOfValidCandy::add);
        return listOfValidCandy;
    }
}
