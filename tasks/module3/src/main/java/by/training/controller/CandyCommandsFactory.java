package by.training.controller;

import by.training.commands.Command;
import by.training.entity.Candy;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class CandyCommandsFactory implements CommandsFactory<Candy, TypeOfBuild> {
    private Map<TypeOfBuild, Command<Candy>> mapOfCommands;

    public CandyCommandsFactory() {
        this.mapOfCommands = new HashMap<>();
    }

    public void putCommand(TypeOfBuild typeOfBuild, Command<Candy> command) {
        mapOfCommands.put(typeOfBuild, command);
    }

    public Command getCommandByType(TypeOfBuild typeOfBuild) {
        return mapOfCommands.get(typeOfBuild);
    }

    Set<TypeOfBuild> getSetOfTypes() {
        return mapOfCommands.keySet();
    }

    public void remove(TypeOfBuild typeOfBuild) {
        mapOfCommands.remove(typeOfBuild);
    }
}
