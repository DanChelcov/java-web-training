package by.training.controller;

public enum TypeOfBuild {
    DOM,
    SAX,
    STAX
}
