package by.training.controller;

import by.training.commands.Command;

public interface CommandsFactory<T, H> {
    Command<T> getCommandByType(H command);
}
