package by.training.controller;

import by.training.commands.CommandException;
import by.training.commands.Command;
import by.training.service.CandyService;
import by.training.validation.FileValidator;
import by.training.validation.ValidationResult;
import by.training.validation.XMLByXSDValidator;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class CandyController {
    private static final Logger LOGGER = LogManager.getLogger(CandyController.class);
    private final CandyService candyService;
    private CommandsFactory candyCommandsFactory;
    private static final String xsdPath = "/Candies.xsd";

    public CandyController(CandyService candyService ,CandyCommandsFactory candyCommandsFactory) {
        this.candyService = candyService;
        this.candyCommandsFactory = candyCommandsFactory;
    }

    public boolean readInfoFromXML(String path, TypeOfBuild typeOfBuild) {
        List candyList = new ArrayList();
        FileValidator fileValidator = new FileValidator();
        ValidationResult validationResult = fileValidator.getValidationResult(path);
        if (validationResult.isValid()) {
            XMLByXSDValidator xmlByXSDValidator = new XMLByXSDValidator(this.getClass().getResource(xsdPath).getPath());
            validationResult = xmlByXSDValidator.getValidationResult(path);
            if (validationResult.isValid()) {
                Command command = candyCommandsFactory.getCommandByType(typeOfBuild);
                try {
                    candyList = command.build(path);
                } catch (CommandException e) {
                    LOGGER.log(Level.ERROR, "cant build Candy List", e);
                }
            } else {
                LOGGER.log(Level.ERROR, "problem with XML :" + validationResult.getAllVales());
            }
        } else {
            LOGGER.log(Level.ERROR, "problem with file :" + validationResult.getAllVales());
        }
        if(!candyList.isEmpty()) {
            candyService.saveListOfItems(candyList);
            return true;
        }else {
            return false;
        }
    }
}
