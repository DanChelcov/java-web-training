package by.training.validation;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

public class XMLByXSDValidator implements ValidatorInt {
    private String xsdPath;

    public XMLByXSDValidator(String xsdPath) {
        this.xsdPath = xsdPath;
    }

    public String getXmlPath() {
        return xsdPath;
    }

    public void setXmlPath(String xsdPath) {
        this.xsdPath = xsdPath;
    }

    @Override
    public ValidationResult getValidationResult(String pathToXML) {
        ValidationResult validationResult = new ValidationResult();
        try {
            SchemaFactory factory = SchemaFactory
                    .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new StreamSource(this.xsdPath));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(pathToXML));
        } catch (Exception e) {
            validationResult.addInfo("XML_Validation", "XML is not valid");
        }
        return validationResult;
    }
}
