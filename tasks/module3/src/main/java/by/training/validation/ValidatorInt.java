package by.training.validation;

public interface ValidatorInt {
    ValidationResult getValidationResult(String path);
}
