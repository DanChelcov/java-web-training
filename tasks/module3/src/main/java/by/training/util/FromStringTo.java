package by.training.util;

public class FromStringTo {
    public static long toLong(String str){
        long num = Long.parseLong(str);
          return num;

    }
    public static int toInt(String str){
        int num = Integer.parseInt(str);
        return num;

    }
    public static boolean toBoolean(String str){
        boolean num = Boolean.parseBoolean(str);
        return num;

    }
}
