package by.training.controller;


import by.training.repository.VanRepository;
import by.training.service.VanServiceImpl;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;


import java.net.URISyntaxException;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class VanControllerTest {
    Logger LOGGER = LogManager.getLogger(this.getClass());
    VanController vanController;
    VanServiceImpl vanServiceImpl;
    VanRepository vanRepository;
    String path = Paths.get(this.getClass().getClassLoader().getResource("vansInfo.txt").toURI()).toString();

    public VanControllerTest() throws URISyntaxException {
    }

    @Before
    public void init() {
        vanRepository = new VanRepository();
        vanServiceImpl = new VanServiceImpl(vanRepository);
        vanController = new VanController(vanServiceImpl);
    }

    @Test
    public void readFromFile() {
        LOGGER.log(Level.INFO, "-------------START READ_FROM_FILE");
        assertTrue(vanController.readVansInfoFromFile(path));
        assertEquals(7, vanServiceImpl.getSetOfVan().size());
        LOGGER.log(Level.INFO, "-------------FINISH READ_FROM_FILE");
    }

}