package by.training.entity;

public class AllMetalWagon extends Van {
    public AllMetalWagon() {
        this.setVanType(VanType.ALL_METAL_WAGON);
    }

    public AllMetalWagon(long id) {
        super(id);
        this.setVanType(VanType.ALL_METAL_WAGON);
    }

    public AllMetalWagon(double vanArea, boolean isLoaded, boolean withPerishableGoods) {
        super(vanArea, isLoaded, withPerishableGoods);
        this.setVanType(VanType.ALL_METAL_WAGON);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "AllMetalWagon{" +
                super.toString() +
                "}";
    }
}
