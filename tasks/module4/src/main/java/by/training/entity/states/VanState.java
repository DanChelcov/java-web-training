package by.training.entity.states;

import by.training.entity.Van;

public interface VanState {
    void next(Van van);
    void prev(Van van);
    String getStatus();
}
