package by.training.entity;

import by.training.entity.states.LoadedState;
import by.training.entity.states.UnloadedState;
import by.training.entity.states.VanState;

import java.util.Objects;

public abstract class Van {
    private long id;
    private double vanArea;
    private VanType vanType;
    private boolean isLoaded;
    private boolean withPerishableGoods;

    private VanState vanState;

    protected Van() {
    }

    public Van(long id) {
        this.id = id;
    }

    protected Van(double vanArea, boolean isLoaded, boolean withPerishableGoods) {
        this.vanArea = vanArea;
        this.isLoaded = isLoaded;
        this.withPerishableGoods = withPerishableGoods;
        vanState = isLoaded ? new LoadedState() : new UnloadedState();
    }
    public void nextState(){
        vanState.next(this);
    }
    public void previousState(){
        vanState.prev(this);
    }
    public String getStatus(){
        return vanState.getStatus();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getVanArea() {
        return vanArea;
    }

    public void setVanArea(double vanArea) {
        this.vanArea = vanArea;
    }

    public VanType getVanType() {
        return vanType;
    }

    protected void setVanType(VanType vanType) {
        this.vanType = vanType;
    }

    public boolean isLoaded() {
        return isLoaded;
    }

    public void setLoaded(boolean loaded) {
        isLoaded = loaded;
        vanState = isLoaded ? new LoadedState() : new UnloadedState();
    }

    public boolean isWithPerishableGoods() {
        return withPerishableGoods;
    }

    public void setWithPerishableGoods(boolean withPerishableGoods) {
        this.withPerishableGoods = withPerishableGoods;
    }

    public void setVanState(VanState vanState){
        this.vanState = vanState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Van van = (Van) o;
        return Double.compare(van.vanArea, vanArea) == 0 &&
                id == van.id &&
                isLoaded == van.isLoaded &&
                withPerishableGoods == van.withPerishableGoods &&
                vanType.equals(van.vanType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, vanArea, vanType, isLoaded, withPerishableGoods);
    }

    @Override
    public String toString() {
        return "id = " + id +
                ", vanArea = " + vanArea +
                ", vanType = " + vanType +
                ", isLoaded = " + isLoaded +
                ", withPerishableGoods = " + withPerishableGoods;
    }
}
