package by.training.entity;

public class LightVan extends Van {
    public LightVan() {
        this.setVanType(VanType.LIGHT_VAN);
    }

    public LightVan(long id) {
        super(id);
        this.setVanType(VanType.LIGHT_VAN);
    }

    public LightVan(double vanArea, boolean isLoaded, boolean withPerishableGoods) {
        super(vanArea, isLoaded, withPerishableGoods);
        this.setVanType(VanType.LIGHT_VAN);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "LightVan{" +
                super.toString() +
                "}";
    }
}
