package by.training.entity.states;

import by.training.entity.Van;

public class LoadedState implements VanState {
    @Override
    public void next(Van van) {
        van.setLoaded(false);
        van.setVanState(new UnloadedState());
    }


    @Override
    public void prev(Van van) {
        van.setLoaded(false);
        van.setVanState(new UnloadedState());
    }

    @Override
    public String getStatus() {
        String status = "Status : is Loaded";
        return status;
    }
}
