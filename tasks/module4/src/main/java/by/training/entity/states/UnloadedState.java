package by.training.entity.states;

import by.training.entity.Van;

public class UnloadedState implements VanState {
    @Override
    public void next(Van van) {
        van.setLoaded(true);
        van.setVanState(new LoadedState());
    }


    @Override
    public void prev(Van van) {
        van.setLoaded(true);
        van.setVanState(new LoadedState());
    }

    @Override
    public String getStatus() {
        String status = "Status : is Unloaded";
        return status;
    }
}
