package by.training.entity;

public enum VanType {
    VAN_BOX,
    ALL_METAL_WAGON,
    LIGHT_VAN
}
