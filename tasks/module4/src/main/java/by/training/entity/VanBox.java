package by.training.entity;

public class VanBox extends Van {
    public VanBox() {
        this.setVanType(VanType.VAN_BOX);
    }

    public VanBox(long id) {
        super(id);
        this.setVanType(VanType.VAN_BOX);
    }

    public VanBox(double vanArea, boolean isLoaded, boolean withPerishableGoods) {
        super(vanArea, isLoaded, withPerishableGoods);
        this.setVanType(VanType.VAN_BOX);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "VanBox{" +
                super.toString() +
                "}";
    }

}
