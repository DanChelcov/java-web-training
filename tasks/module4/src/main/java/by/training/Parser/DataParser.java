package by.training.Parser;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class DataParser {
    private static final String DATA_SPLITTER = ";";
    private static final String UNO_DATA_SPLITTER = ":";

    public List<Map<String, String>> getDataListOfMaps(List<String> dataListOfString) {
        if (dataListOfString == null) {
            return null;
        }
        List<Map<String, String>> dataListOfMaps = new LinkedList<>();
        for (String infoLine : dataListOfString) {
            dataListOfMaps.add(getDataMap(infoLine));
        }
        return dataListOfMaps;
    }

    public Map<String, String> getDataMap(String dataLine) {
        Map<String, String> dataMap = new HashMap<>();
        String[] data = dataLine.split(DATA_SPLITTER);
        for (String unoDataLine : data) {
            String[] unoData = unoDataLine.split(UNO_DATA_SPLITTER);
            dataMap.put(unoData[0].trim(), unoData[1].trim());
        }
        return dataMap;
    }
}
