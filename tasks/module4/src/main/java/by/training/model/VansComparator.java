package by.training.model;

import by.training.entity.Van;

import java.util.Comparator;

public class VansComparator implements Comparator<Van> {
    @Override
    public int compare(Van van, Van t1) {
        return Boolean.compare(t1.isWithPerishableGoods(), van.isWithPerishableGoods());
    }
}
