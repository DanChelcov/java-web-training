package by.training.model;

import by.training.entity.Van;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LogisticBase {

    private static Lock creatorLock = new ReentrantLock();
    private static LogisticBase instance = null;
    private final static Logger LOGGER = LogManager.getLogger(LogisticBase.class);
    private final int NUMBER_OF_TERMINALS;
    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();

    private AtomicBoolean clientsOutside;
    private Queue<Van> vanQueue;
    private ExecutorService service;

    private LogisticBase(int NUMBER_OF_TERMINALS) {
        this.NUMBER_OF_TERMINALS = NUMBER_OF_TERMINALS;
        vanQueue = new LinkedList<>();
        service = Executors.newFixedThreadPool(NUMBER_OF_TERMINALS);
        clientsOutside = new AtomicBoolean(false);
    }

    public static LogisticBase getInstance(int numberOfTerminals) {
        creatorLock.lock();
        try {
            if (instance == null) {
                instance = new LogisticBase(numberOfTerminals);
                return instance;
            } else {
                return instance;
            }
        } finally {
            creatorLock.lock();
        }

    }

    public boolean isThereAnyClients(){
        lock.lock();
        try {
           return !vanQueue.isEmpty() ||  clientsOutside.get();
        }finally {
            lock.unlock();
        }
    }

    public void setClientsOutside(boolean clientsOutside) {
        lock.lock();
        try {
            this.clientsOutside.set(clientsOutside);
        } finally {
            lock.unlock();
        }
    }


    public void startBaseWork() {
        LOGGER.log(Level.INFO, "-----LOGISTIC_BASE HAS STARTED WORK------");
        for (int i = 1; i <= NUMBER_OF_TERMINALS; i++) {
            service.execute(new Terminal(this));
        }
    }

    public void putVanInside(Van van) {
        lock.lock();
        try {
            while (vanQueue.size() >= 4) {
                condition.await();
            }
            vanQueue.add(van);
            LOGGER.log(Level.INFO, "van has arrived to base : id = " + van.getId() + ", with withPerishableGoods : " +
                    van.isWithPerishableGoods() + ", " + van.getStatus());
            condition.signalAll();
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            LOGGER.log(Level.ERROR, e);
        } finally {
            lock.unlock();
        }
    }

    public void shutdownBase() {
        service.shutdownNow();
        LOGGER.log(Level.INFO, "-----LOGISTIC_BASE HAS FINISHED WORK------");
    }

    public Van getToTerminal() {
        lock.lock();
        try {
            while (vanQueue.size() < 1) {
                condition.await();
            }
            condition.signalAll();
            TimeUnit.SECONDS.sleep(1);
            Van van = vanQueue.poll();
            LOGGER.log(Level.INFO, "van(id = " + van.getId() + ") got to terminal : " + Thread.currentThread().getName());
            return van;
        } catch (InterruptedException e) {
            return null;
        } finally {
            lock.unlock();
        }
    }

}
