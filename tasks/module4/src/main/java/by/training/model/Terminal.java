package by.training.model;


import by.training.entity.Van;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class Terminal implements Runnable {
    private final static Logger LOGGER = LogManager.getLogger(Terminal.class);
    private final LogisticBase logisticBase;

    public Terminal(LogisticBase logisticBase) {
        this.logisticBase = logisticBase;
    }

    @Override
    public void run() {
        while (logisticBase.isThereAnyClients()) {

            Van van = logisticBase.getToTerminal();
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {

            }
            if (van == null) {
                break;
            }

            van.nextState();

            LOGGER.log(Level.INFO, "van has served : id = " + van.getId() + ", " + van.getStatus() + "\n");
        }

    }
}
