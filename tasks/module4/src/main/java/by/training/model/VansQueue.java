package by.training.model;

import by.training.entity.Van;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class VansQueue implements Runnable {
    private final LogisticBase logisticBase;
    private Queue<Van> vanQueue;

    public VansQueue(LogisticBase logisticBase, Set<Van> vanSet, Comparator<Van> comparator) {
        this.logisticBase = logisticBase;
        vanQueue = new PriorityQueue<>(comparator);
        vanSet.forEach(vanQueue::add);
        if(!vanQueue.isEmpty()){
            logisticBase.setClientsOutside(true);
        }
    }
    public Queue<Van> getVanQueue(){
        return vanQueue;
    }


    @Override
    public void run() {
        while (!vanQueue.isEmpty()){
            logisticBase.putVanInside(vanQueue.poll());
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {

            }
        }
      logisticBase.setClientsOutside(false);
    }
}
