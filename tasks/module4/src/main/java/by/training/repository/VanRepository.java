package by.training.repository;

import by.training.entity.Van;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

public class VanRepository implements Repository<Van> {
    AtomicLong idSetter = new AtomicLong(1);
    private Set<Van> vanSet;

    public VanRepository() {
        vanSet = new HashSet<>();
    }

    @Override
    public long add(Van item) {
        item.setId(idSetter.getAndIncrement());
        vanSet.add(item);
        return item.getId();
    }

    @Override
    public boolean delete(long id) {
        boolean result = false;
        for (Van van : vanSet) {
            if (id == van.getId()) {
                vanSet.remove(van);
                result = true;
            }
        }
        return result;
    }

    @Override
    public Van read(long id) throws IncorrectIdException {
        for (Van van1 : vanSet) {
            if (id == van1.getId()) {
                return van1;
            }
        }
        throw new IncorrectIdException("Cant find van by this id", id);
    }


    @Override
    public Set<Van> getSetOfItems() {
        return new HashSet<>(vanSet);
    }

    @Override
    public Set<Van> findBySpec(Specification<Van> spec) {
        Set<Van> listOfValidVan = new HashSet<>();
        vanSet.stream().filter(spec::isSatisfiedBy).forEach(listOfValidVan::add);
        return listOfValidVan;
    }
}
