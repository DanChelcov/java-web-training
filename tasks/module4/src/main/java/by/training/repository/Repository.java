package by.training.repository;

import java.util.Set;

public interface Repository<T> {
    long add(T item);

    boolean delete(long id);

    T read(long id) throws IncorrectIdException;

    Set<T> getSetOfItems();

    Set<T> findBySpec(Specification<T> spec);
}
