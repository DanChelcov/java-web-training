package by.training.controller;

public class ReaderException extends Exception {
    public Exception exception;

    public ReaderException() {

    }

    public ReaderException(String message) {
        super(message);
    }

    public ReaderException(String message, Exception e) {
        super(message);
        this.exception = e;
    }
}
