package by.training.controller;

public class BuilderFactoryException extends RuntimeException {
    Exception exception;
    public BuilderFactoryException(String message) {
        super(message);
    }

    public BuilderFactoryException(String message, Exception e) {
        super(message);
        this.exception = e;
    }
}
