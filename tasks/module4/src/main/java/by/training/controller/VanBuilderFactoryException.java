package by.training.controller;

public class VanBuilderFactoryException extends BuilderFactoryException {

    public VanBuilderFactoryException(String message) {
        super(message);
    }

    public VanBuilderFactoryException(String message, Exception e) {
        super(message, e);
    }
}
