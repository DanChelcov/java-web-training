package by.training.controller;

import by.training.builder.Builder;

import java.util.Map;

public interface BuilderFactory<T>  {
    Builder<? extends T> getBuilder(Map<String, String> dataMap) throws BuilderFactoryException;
}
