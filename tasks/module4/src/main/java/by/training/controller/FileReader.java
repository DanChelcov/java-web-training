package by.training.controller;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileReader {
    private static final String FILE_ERROR = "Problem with file";
    private final static Logger LOGGER = LogManager.getLogger(FileReader.class);

    public List<String> readAllData(String path) throws ReaderException {
        try {
            return Files.readAllLines(Paths.get(path));
        } catch (IOException e) {
            LOGGER.log(Level.ERROR, FILE_ERROR, e);
            throw new ReaderException(FILE_ERROR, e);
        }
    }
}
