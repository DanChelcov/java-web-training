package by.training.controller;

import by.training.builder.AllMetalWagonBuilder;
import by.training.builder.Builder;
import by.training.builder.LightVanBuilder;
import by.training.builder.VanBoxBuilder;
import by.training.entity.Van;
import by.training.entity.VanType;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Map;

public class VanBuilderFactory implements BuilderFactory<Van> {
    private static final String VAN_TYPE_KEY = "vanType";
    private final static Logger LOGGER = LogManager.getLogger(VanBuilderFactory.class);

    @Override
    public Builder<? extends Van> getBuilder(Map<String, String> dataMap) {
        String vanType = dataMap.get(VAN_TYPE_KEY).toUpperCase();
        switch (VanType.valueOf(vanType)) {
            case VAN_BOX:
                return new VanBoxBuilder();
            case LIGHT_VAN:
                return new LightVanBuilder();
            case ALL_METAL_WAGON:
                return new AllMetalWagonBuilder();
            default:
                LOGGER.log(Level.ERROR, "map doesn`t have \\`vanType\\' ");
                throw new VanBuilderFactoryException("Cant create builder. VanType wasn`t match");
        }
    }
}
