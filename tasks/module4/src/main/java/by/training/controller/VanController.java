package by.training.controller;

import by.training.Parser.DataParser;
import by.training.entity.Van;
import by.training.service.VanServiceImpl;
import by.training.validation.FileValidator;
import by.training.validation.ValidationResult;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class VanController {
    private static final Logger LOGGER = LogManager.getLogger(VanController.class);
    private final VanServiceImpl vanServiceImpl;
    private BuilderFactory<Van> builderFactory;
    private FileReader fileReader;
    private DataParser dataParser;

    public VanController(VanServiceImpl vanServiceImpl) {
        this.vanServiceImpl = vanServiceImpl;
        builderFactory = new VanBuilderFactory();
        fileReader = new FileReader();
        dataParser = new DataParser();
    }

    public boolean readVansInfoFromFile(String path) {
        FileValidator fileValidator = new FileValidator();
        ValidationResult validationResult = fileValidator.getValidationResult(path);
        if (!validationResult.isValid()) {
            LOGGER.log(Level.ERROR, "problem with file : " + validationResult.getResult());
            return false;
        }
        List<String> dataList;
        try {
            dataList = fileReader.readAllData(path);
        } catch (ReaderException e) {
            LOGGER.log(Level.ERROR, "cant read file", e);
            return false;
        }
        List<Map<String, String>> infoList = dataParser.getDataListOfMaps(dataList);
        infoList.stream().
                map(info -> builderFactory.getBuilder(info).build(info)).
                forEach(vanServiceImpl::saveVan);

        return true;
    }
    public Set<Van> getSetOfVans(){
        return vanServiceImpl.getSetOfVan();
    }
}
