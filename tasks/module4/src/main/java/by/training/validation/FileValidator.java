package by.training.validation;

import java.io.File;

public class FileValidator implements ValidatorInt {

    public ValidationResult getValidationResult(String path) {
        ValidationResult validationResult = new ValidationResult();
        if (path == null) {
            validationResult.addInfo("null", "path is null");
            return validationResult;
        }
        File file = new File(path);
        if (!file.exists()) {
            validationResult.addInfo("file_exist ", " file is not exist");
            return validationResult;
        }
        if (file.length() == 0) {
            validationResult.addInfo("file_size ", " file is empty");
        }
        return validationResult;
    }


}

