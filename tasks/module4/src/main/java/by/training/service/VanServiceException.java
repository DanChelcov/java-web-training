package by.training.service;

public class VanServiceException extends Exception {
    public Exception exception;

    public VanServiceException() {

    }

    public VanServiceException(String message) {
        super(message);
    }

    public VanServiceException(String message, Exception e) {
        super(message);
        this.exception = e;
    }
}
