package by.training.service;

public class VanServiceImplException extends VanServiceException {


    public VanServiceImplException() {

    }

    public VanServiceImplException(String message) {
        super(message);
    }

    public VanServiceImplException(String message, Exception e) {
        super(message, e);
    }
}
