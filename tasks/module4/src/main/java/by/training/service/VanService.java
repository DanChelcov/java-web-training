package by.training.service;

import by.training.entity.Van;

import java.util.Comparator;
import java.util.List;
import java.util.Set;

public interface VanService {

    long saveVan(Van van);
    boolean deleteVan(long id);
    Van takeVan(long id) throws VanServiceException;
    Set<Van> getSetOfVan();
    List<Van> getSortedListOfVans(Comparator<Van> comparator) throws VanServiceException;
}
