package by.training.service;

import by.training.entity.Van;
import by.training.repository.IncorrectIdException;
import by.training.repository.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;


public class VanServiceImpl implements VanService {
    private final Repository<Van> vanRepository;

    public VanServiceImpl(Repository<Van> vanRepository) {
        this.vanRepository = vanRepository;
    }

    public long saveVan(Van van) {
        return vanRepository.add(van);
    }

    public boolean deleteVan(long id) {
        return vanRepository.delete(id);
    }

    public Van takeVan(long id) throws VanServiceImplException {
        try {
            return vanRepository.read(id);
        } catch (IncorrectIdException e) {
            throw new VanServiceImplException("service cant take van", e);
        }
    }

    public Set<Van> getSetOfVan() {
        return vanRepository.getSetOfItems();
    }

    public List<Van> getSortedListOfVans(Comparator<Van> comparator) throws VanServiceImplException {
        if (comparator == null) throw new VanServiceImplException("cant sort vans : comparator is null");
        List<Van> vanList = new ArrayList<>(vanRepository.getSetOfItems());
        vanList.sort(comparator);
        return vanList;
    }

}
