package by.training.builder;

import java.util.Map;

public interface Builder<T> {
    T build(Map<String, String> dataMap);


}
