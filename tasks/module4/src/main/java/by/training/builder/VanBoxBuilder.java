package by.training.builder;

import by.training.entity.Van;
import by.training.entity.VanBox;

import java.util.Map;

public class VanBoxBuilder implements Builder<Van> {
    private static final String VAN_AREA_KEY = "vanArea";
    private static final String IS_LOADED_KEY = "isLoaded";
    private static final String WITH_PERISHABLE_GOODS_KEY = "withPerishableGoods";

    @Override
    public Van build(Map<String, String> dataMap) {
        double vanArea = Double.parseDouble(dataMap.get(VAN_AREA_KEY));
        boolean isLoaded = Boolean.parseBoolean(dataMap.get(IS_LOADED_KEY));
        boolean withPerishableGoods = Boolean.parseBoolean(dataMap.get(WITH_PERISHABLE_GOODS_KEY));
        return new VanBox(vanArea, isLoaded, withPerishableGoods);
    }
}
