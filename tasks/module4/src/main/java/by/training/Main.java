package by.training;

import by.training.controller.VanController;
import by.training.entity.Van;
import by.training.model.LogisticBase;
import by.training.model.VansComparator;
import by.training.model.VansQueue;
import by.training.repository.VanRepository;
import by.training.service.VanServiceImpl;

import java.io.File;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) throws  InterruptedException {
        String path = new File(Main.class.getClassLoader().getResource("vansInfo.txt").getFile()).getAbsolutePath();
        VanRepository vanRepository = new VanRepository();
        VanServiceImpl vanServiceImpl = new VanServiceImpl(vanRepository);
        VanController vanController = new VanController(vanServiceImpl);
        vanController.readVansInfoFromFile(path);
        Set<Van> set = vanController.getSetOfVans();


        VansComparator vansComparator = new VansComparator();
        LogisticBase logisticBase = LogisticBase.getInstance(4);
        VansQueue vansQueue = new VansQueue(logisticBase, set, vansComparator);
        Thread thread = new Thread(vansQueue);
        logisticBase.startBaseWork();
        TimeUnit.SECONDS.sleep(2);
        thread.start();

        while (true) {
            if (!logisticBase.isThereAnyClients()) {
               TimeUnit.SECONDS.sleep(3);
                logisticBase.shutdownBase();
                break;
            }
        }


    }
}
