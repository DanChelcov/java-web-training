package by.chelcov.taxipark.controller;


import by.chelcov.taxipark.entity.Car;
import by.chelcov.taxipark.entity.CarCreatorException;
import by.chelcov.taxipark.service.TaxiparkCreatorService;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CreatorTest {      //4 объекта в таксопарке создается по дефолту

    @Test
    public void taxiparkCreatorService1() throws CarCreatorException, ControllerException, ProblemWithFileException {
        Creator creatorTest = new Creator(this.getClass().getResource("/taxiPark1.txt").getPath());
        TaxiparkCreatorService taxiparkCreatorService = creatorTest.createTaxiparkCreatorService();
        List<Car> list = taxiparkCreatorService.createTaxipark().getListCar();
        assertEquals(7, list.size());
    }

    @Test
    public void taxiparkCreatorService2() throws CarCreatorException, ControllerException, ProblemWithFileException {
        Creator creatorTest = new Creator(this.getClass().getResource("/taxiPark2.txt").getPath());
        TaxiparkCreatorService taxiparkCreatorService = creatorTest.createTaxiparkCreatorService();
        List<Car> list = taxiparkCreatorService.createTaxipark().getListCar();
        assertEquals(7, list.size());
    }
    @Test
    public void taxiparkCreatorService3() throws CarCreatorException, ControllerException,  ProblemWithFileException {
        Creator creatorTest = new Creator(this.getClass().getResource("/taxiPark3.txt").getPath());
        TaxiparkCreatorService taxiparkCreatorService = creatorTest.createTaxiparkCreatorService();
        List<Car> list = taxiparkCreatorService.createTaxipark().getListCar();
        assertEquals(4, list.size());

    }



}