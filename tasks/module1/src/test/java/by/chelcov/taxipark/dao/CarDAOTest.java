package by.chelcov.taxipark.dao;

import by.chelcov.taxipark.entity.*;
import by.chelcov.taxipark.service.CarServiceException;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;


public class CarDAOTest {


    @Test
    public void addCar() throws CarCreatorException, CarServiceException {
        DAOTaxipark daoTaxipark = new DAOTaxipark();
        daoTaxipark.addCar(new Taxi(400, 12, 213, CarModel.BUICK, TypeOfTaxi.BUSINESS));
        daoTaxipark.addCar(new Taxi(500, 11, 203, CarModel.BUICK, TypeOfTaxi.BUSINESS));
        daoTaxipark.addCar(new FreightTaxi(400, 12, 213, CarModel.BUICK, TypeOfShipping.OFFICE_SHIPPING));
        daoTaxipark.addCar(new Taxi(400, 12, 213, CarModel.BUICK, TypeOfTaxi.BUSINESS));
        assertEquals(8, daoTaxipark.getListCar().size());
    }

    @Test
    public void removeCar() throws CarCreatorException, CarServiceException {
        DAOTaxipark daoTaxipark = new DAOTaxipark();
        daoTaxipark.getListCar().add(new Taxi(400, 12, 213, CarModel.BUICK, TypeOfTaxi.BUSINESS));
        assertEquals(5,daoTaxipark.getListCar().size());
        daoTaxipark.removeCar(new Taxi(400, 12, 213, CarModel.BUICK, TypeOfTaxi.BUSINESS));
        assertEquals(4,daoTaxipark.getListCar().size());
    }
    @Test(expected = CarCreatorException.class)
    public void removeCarEx() throws CarCreatorException, CarServiceException {
        DAOTaxipark daoTaxipark = new DAOTaxipark();
        daoTaxipark.getListCar().add(new Taxi(-400, 12, 213, CarModel.BUICK, TypeOfTaxi.BUSINESS));
        assertEquals(5,daoTaxipark.getListCar().size());
        daoTaxipark.removeCar(new Taxi(400, 12, 213, CarModel.BUICK, TypeOfTaxi.BUSINESS));
        assertEquals(4,daoTaxipark.getListCar().size());
    }
    @Test(expected = CarServiceException.class)
    public void removeCarEx2() throws CarCreatorException, CarServiceException {
        DAOTaxipark daoTaxipark = new DAOTaxipark();
        daoTaxipark.getListCar().add(new Taxi(400, 12, 213, CarModel.BUICK, TypeOfTaxi.BUSINESS));
        assertEquals(5,daoTaxipark.getListCar().size());
        daoTaxipark.removeCar(null);
        assertEquals(4,daoTaxipark.getListCar().size());
    }
}