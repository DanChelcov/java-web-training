package by.chelcov.taxipark.dao;

import by.chelcov.taxipark.entity.Car;
import by.chelcov.taxipark.entity.CarCreatorException;
import by.chelcov.taxipark.service.ManagerServiceException;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class ManagerDAOTest {
    @Test
    public void scoreCost() throws CarCreatorException {
        DAOTaxipark daoTaxipark = new DAOTaxipark();
        assertEquals(14190, daoTaxipark.scoreCost());
    }

    @Test
    public void findOnDiapasonOfCarrying() throws CarCreatorException, ManagerServiceException {
        DAOTaxipark daoTaxipark = new DAOTaxipark();
        List<Car> list = daoTaxipark.findOnDiapasonOfCarrying(100,1000);
        assertEquals(2, list.size());
        for(Car car : list){
            System.out.println(car.toString() + "\n");
        }

    }

    @Test
    public void findOnDiapasonOfHolding() throws ManagerServiceException, CarCreatorException {
        DAOTaxipark daoTaxipark = new DAOTaxipark();
        List<Car> list = daoTaxipark.findOnDiapasonOfHolding(30,32);
        assertEquals(2, list.size());
        for(Car car : list){
            System.out.println(car.toString() + "\n");
        }

    }

    @Test
    public void findOnDiapasonOfCarryingAndHolding() throws ManagerServiceException, CarCreatorException {
        DAOTaxipark daoTaxipark = new DAOTaxipark();
        List<Car> list = daoTaxipark.findOnDiapasonOfCarryingAndHolding(900,7000,5,35 );
        assertEquals(3, list.size());
        for(Car car : list){
            System.out.println(car.toString() + "\n");
        }

    }
}