package by.chelcov.taxipark.dao;

import by.chelcov.taxipark.entity.CarCreatorException;
import org.junit.Test;

import static org.junit.Assert.*;

public class SorterDAOTest {

    @Test
    public void sortByCarryingCap() throws CarCreatorException {
        DAOTaxipark daoTaxipark = new DAOTaxipark();
        daoTaxipark.sortByCarryingCap();
        assertEquals(6780, daoTaxipark.getListCar().get(0).getCarryingCapacity());
        assertEquals(1240, daoTaxipark.getListCar().get(1).getCarryingCapacity());
        assertEquals(984, daoTaxipark.getListCar().get(2).getCarryingCapacity());
        assertEquals(124, daoTaxipark.getListCar().get(3).getCarryingCapacity());
    }

    @Test
    public void sortByHoldingCap() throws CarCreatorException {
        DAOTaxipark daoTaxipark = new DAOTaxipark();
        daoTaxipark.sortByHoldingCap();
        assertEquals(32, daoTaxipark.getListCar().get(0).getHoldingCapacity());
        assertEquals(30, daoTaxipark.getListCar().get(1).getHoldingCapacity());
        assertEquals(6, daoTaxipark.getListCar().get(2).getHoldingCapacity());
        assertEquals(3, daoTaxipark.getListCar().get(3).getHoldingCapacity());
    }

    @Test
    public void sortByCost() throws CarCreatorException {
        DAOTaxipark daoTaxipark = new DAOTaxipark();
        daoTaxipark.sortByCost();
        assertEquals(5900, daoTaxipark.getListCar().get(0).getCost());
        assertEquals(4900, daoTaxipark.getListCar().get(1).getCost());
        assertEquals(3200, daoTaxipark.getListCar().get(2).getCost());
        assertEquals(190, daoTaxipark.getListCar().get(3).getCost());
    }
}