package by.chelcov.taxipark.dao;

import by.chelcov.taxipark.entity.Car;
import by.chelcov.taxipark.service.CarServiceException;

public interface CarDAOInt {
    void addCar(Car car) throws CarServiceException, CarMoveException;

    void removeCar(Car car) throws CarServiceException;
}
