package by.chelcov.taxipark.controller;

import java.util.List;
import java.util.Map;

public interface CarTypeValidate {
    Map<String, List<String>> ValidateType(String[] arrayOfStr, int counter);
}
