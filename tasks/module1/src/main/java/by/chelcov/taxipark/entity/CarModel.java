package by.chelcov.taxipark.entity;

public enum CarModel {
    FORD,
    RENAULT,
    MAZDA,
    VOLKSWAGEN,
    BUICK,
    UNKNOWN
}
