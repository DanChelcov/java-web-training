package by.chelcov.taxipark.service;

import by.chelcov.taxipark.dao.DAOTaxipark;

public class SorterService implements SorterServiceInt {
    private DAOTaxipark daoTaxipark;

    public SorterService(DAOTaxipark daoTaxipark) {
        this.daoTaxipark = daoTaxipark;
    }

    public void sortByCarryingCap() {
        daoTaxipark.sortByCarryingCap();
    }

    public void sortByHoldingCap() {
        daoTaxipark.sortByHoldingCap();
    }

    public void sortByCost() {
        daoTaxipark.sortByCost();
    }
}
