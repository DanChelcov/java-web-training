package by.chelcov.taxipark.controller;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileValidator implements Validate {
    public Map<String, List<String>> informationAboutErr;

    private FileValidator() {
        informationAboutErr = new HashMap<>();
    }

    public static FileValidator getFileValidator(String line) {
        FileValidator fileValidator = new FileValidator();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(line))) {
            if (!bufferedReader.ready()) throw new FileIsEmptyException("File is empty");
        } catch (FileNotFoundException e) {
            ExcWorker.workWithExc(e, fileValidator.informationAboutErr);
        } catch (IOException e) {
            ExcWorker.workWithExc(e, fileValidator.informationAboutErr);

        } catch (FileIsEmptyException e) {
            fileValidator.informationAboutErr.put("FILE_IS_EMPTY", new ArrayList<String >());

        }

        return fileValidator;
    }


    @Override
    public boolean isValid() {
        return informationAboutErr.isEmpty();
    }
}
