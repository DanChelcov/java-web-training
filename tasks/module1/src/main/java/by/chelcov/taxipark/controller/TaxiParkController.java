package by.chelcov.taxipark.controller;


import by.chelcov.taxipark.entity.Car;
import by.chelcov.taxipark.entity.CarCreatorException;
import by.chelcov.taxipark.service.TaxiparkCreatorService;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Creator {
    private final String FILE_NAME;
    public static final Logger LOGGER = LogManager.getLogger(Creator.class);


    public Creator(String file_name) {
        FILE_NAME = file_name;
    }

    public TaxiparkCreatorService createTaxiparkCreatorService() throws  ProblemWithFileException {
        List<Car> carList = new ArrayList<>();
        List<String> listOfStrCars = new ArrayList<>();
        FileValidator fileValidator = FileValidator.getFileValidator(FILE_NAME);
        if (fileValidator.isValid()) {
            Map<String, List<String>> mapOfProb = new HashMap<>();
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(FILE_NAME))) {
                int counter = 0;
                while (bufferedReader.ready()) {
                    counter += 1;
                    String line = bufferedReader.readLine();
                    InfoValidator infoValidator = InfoValidator.checkForInfoValidation(line, counter);
                    if (infoValidator.isValid()) {
                        listOfStrCars.add(line);
                        LOGGER.log(Level.INFO, " line : " + counter + " - has successfully entered");
                    } else {
                        LOGGER.log(Level.INFO, infoValidator.faultInfo);

                    }
                }
            } catch (FileNotFoundException e) {
                LOGGER.log(Level.FATAL, e);
            } catch (IOException e) {
                LOGGER.log(Level.FATAL, e);
            }

        } else {
            if(fileValidator.informationAboutErr.containsKey("FILE_IS_EMPTY")) return new TaxiparkCreatorService();
            LOGGER.log(Level.ERROR, fileValidator.informationAboutErr);
            throw new ProblemWithFileException();
        }

        if (listOfStrCars.isEmpty()) {
            LOGGER.log(Level.INFO, "CreateService has created without info from file");
            return new TaxiparkCreatorService();
        } else {
            for (String line : listOfStrCars) {
                try {
                    carList.add(new CarParser(line).getCarBuilder().buildCar());
                }catch (CarCreatorException e){
                    LOGGER.log(Level.FATAL, "can`t create car");
                }
            }
            LOGGER.log(Level.INFO, "CreateService has successfully created ");
            return new TaxiparkCreatorService(carList);
        }
    }
}
