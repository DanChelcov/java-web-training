package by.chelcov.taxipark.controller;


import by.chelcov.taxipark.controller.builder.CarBuilder;
import by.chelcov.taxipark.controller.builder.FreightTaxiBuilder;
import by.chelcov.taxipark.controller.builder.MinibusBuilder;
import by.chelcov.taxipark.controller.builder.TaxiBuilder;
import by.chelcov.taxipark.entity.CarType;

public class CarParser {
    private String line;

    public CarParser(String line) {
        this.line = line;
    }

    public CarBuilder getCarBuilder() {
        CarBuilder carBuilder;
        line = line.replace(" ", "");
        line = line.replace(":", ",");
        String[] arrayOfStr = line.split("[,]+");
        switch (CarType.valueOf(arrayOfStr[1].toUpperCase())) {
            case TAXI:
                carBuilder = new TaxiBuilder(arrayOfStr);
                break;

            case MINIBUS:
                carBuilder = new MinibusBuilder(arrayOfStr);
                break;
            case FREIGHT_TAXI:
                carBuilder = new FreightTaxiBuilder(arrayOfStr);
                break;
            default:
                carBuilder = null;
        }
        return carBuilder;
    }
}
