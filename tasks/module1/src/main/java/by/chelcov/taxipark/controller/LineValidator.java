package by.chelcov.taxipark.controller;

public class LineValidator implements Validate {
    private String line;
    private final int TAXI_VALIDATE_COUNT = 12;
    private final int MINIBUS_VALIDATE_COUNT = 12;
    private final int FREIGHT_TAX_VALIDATE_COUNT = 12;

    public LineValidator(String line) {
        this.line = line;
    }

    @Override
    public boolean isValid() {
        line = line.replace(" ", ",");
        line = line.replace(":", ",");
        String[] arrayOfStr = line.split("[,]+");
        return arrayOfStr.length == TAXI_VALIDATE_COUNT || arrayOfStr.length == MINIBUS_VALIDATE_COUNT ||
                arrayOfStr.length == FREIGHT_TAX_VALIDATE_COUNT;
    }
}
