package by.chelcov.taxipark.controller.builder;

public class CarBuildException extends Exception {
    public CarBuildException() {
    }

    public CarBuildException(String message) {
        super(message);
    }

    public CarBuildException(String message, Throwable cause) {
        super(message, cause);
    }

    public CarBuildException(Throwable cause) {
        super(cause);
    }
}
