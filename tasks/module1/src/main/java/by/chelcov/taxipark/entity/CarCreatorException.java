package by.chelcov.taxipark.entity;

public class CarCreatorException extends Exception {
    public CarCreatorException() {
    }

    public CarCreatorException(String message) {
        super(message);
    }

    public CarCreatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public CarCreatorException(Throwable cause) {
        super(cause);
    }
}
