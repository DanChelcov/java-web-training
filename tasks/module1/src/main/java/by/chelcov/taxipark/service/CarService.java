package by.chelcov.taxipark.service;

import by.chelcov.taxipark.controller.ControllerException;
import by.chelcov.taxipark.dao.DAOTaxipark;
import by.chelcov.taxipark.entity.Car;


public class CarService implements CarServiceInt {
    private DAOTaxipark daoTaxipark;

    public CarService(DAOTaxipark daoTaxipark) {
        this.daoTaxipark = daoTaxipark;
    }

    public void addCar(Car car) throws ControllerException {
        try {
            daoTaxipark.addCar(car);
        } catch (CarServiceException e){
            throw new ControllerException(e);
        }
    }

    public void removeCar(Car car) throws ControllerException  {
        try {
            daoTaxipark.removeCar(car);
        } catch (CarServiceException e){
            throw new ControllerException(e);
        }

    }
}
