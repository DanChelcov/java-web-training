package by.chelcov.taxipark.controller.builder;

import by.chelcov.taxipark.entity.*;
import by.chelcov.taxipark.util.IntParser;

public class MinibusBuilder implements CarBuilder {
    private final String[] array;

    public MinibusBuilder(String[] array) {
        this.array = array;
    }

    @Override
    public Car buildCar() throws CarCreatorException {
        return new Minibus(IntParser.ParseStrToInt(array[3]), IntParser.ParseStrToInt(array[5]), IntParser.ParseStrToInt(array[7]),
                CarModel.valueOf(array[9].toUpperCase()), TypeOfMinibus.valueOf(array[11].toUpperCase()));
    }
}
