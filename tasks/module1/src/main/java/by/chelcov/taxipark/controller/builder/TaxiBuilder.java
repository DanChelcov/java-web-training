package by.chelcov.taxipark.controller.builder;

import by.chelcov.taxipark.entity.*;
import by.chelcov.taxipark.util.IntParser;

public class TaxiBuilder implements CarBuilder {
    private final String[] array;

    public TaxiBuilder(String[] array) {
        this.array = array;
    }

    @Override
    public Car buildCar() throws CarCreatorException {

       return new Taxi(IntParser.ParseStrToInt(array[3]), IntParser.ParseStrToInt(array[5]), IntParser.ParseStrToInt(array[7]),
                CarModel.valueOf(array[9].toUpperCase()), TypeOfTaxi.valueOf(array[11].toUpperCase()));

    }
}
