package by.chelcov.taxipark.entity;

public enum CarType {
    TAXI,
    MINIBUS,
    FREIGHT_TAXI
}
