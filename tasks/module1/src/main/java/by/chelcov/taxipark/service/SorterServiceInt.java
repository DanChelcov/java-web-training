package by.chelcov.taxipark.service;

public interface SorterServiceInt {
    void sortByCarryingCap();
    void sortByHoldingCap();
    void sortByCost();
}
