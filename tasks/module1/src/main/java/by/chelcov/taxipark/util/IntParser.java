package by.chelcov.taxipark.util;

public class IntParser {
    public static int ParseStrToInt(String str) {
        int result;
        try {
            result = Integer.parseInt(str);
        } catch (NumberFormatException e) {
          result = -1;
        }
        return result;
    }
}
