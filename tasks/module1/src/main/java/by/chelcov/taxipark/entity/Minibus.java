package by.chelcov.taxipark.entity;



public class Minibus extends Car {
private TypeOfMinibus type;

    public Minibus(int carryingCapacity, int holdingCapacity,int cost, CarModel model, TypeOfMinibus type) throws CarCreatorException {
        super(carryingCapacity, holdingCapacity,cost, model);
        carType = CarType.MINIBUS;
        this.type = type;
    }
    public TypeOfMinibus getType() {
        return type;
    }

    public void setType(TypeOfMinibus type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Minibus minibus = (Minibus) o;
        return type.equals(minibus.type) ;
    }

    @Override
    public int hashCode() {
        int result;
        return result = 31 + super.hashCode() + type.hashCode();
    }

    @Override
    public String toString() {
        return "Minibus{" +
                "type = " + type + ", " +
                super.toString() +
                '}';
    }

}
