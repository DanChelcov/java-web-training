package by.chelcov.taxipark.dao;

public interface SortDAOInt {
    void sortByCarryingCap();
    void sortByHoldingCap();
    void sortByCost();
}
