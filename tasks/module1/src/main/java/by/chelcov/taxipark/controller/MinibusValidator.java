package by.chelcov.taxipark.controller;

import by.chelcov.taxipark.entity.TypeOfMinibus;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MinibusValidator implements CarTypeValidate {
    @Override
    public Map<String, List<String>> ValidateType(String[] arrayOfStr, int counter) {
        Map<String, List<String>> map = new CarValidator().validateCar(arrayOfStr, counter);
        List<String> list = new ArrayList<>();
        if (!arrayOfStr[10].equalsIgnoreCase("TypeOfMinibus")) {
            list.add("tenth word is incorrect,it should be -TypeOfMinibus-");
        }
        try {
            TypeOfMinibus.valueOf(arrayOfStr[11].toUpperCase());
        } catch (Exception e) {
            list.add("incorrect TypeOfMinibus of car");
        }
        if (list.isEmpty()) {
            return map;
        }

        map.put("problem with line :" + counter, list);


        return map;
    }
}

