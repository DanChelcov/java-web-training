package by.chelcov.taxipark.controller;

import by.chelcov.taxipark.entity.CarModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CarValidator {

    public Map<String, List<String>> validateCar(String[] arrayOfStr, int counter) {
        Map<String, List<String>> map = new HashMap<>();
        List<String> list = new ArrayList<>();
        if (!arrayOfStr[0].equalsIgnoreCase("CarType")) {
            list.add("first word is incorrect,it should be -CarType-");
        }
        if (!arrayOfStr[2].equalsIgnoreCase("CarryingCapacity")) {
            list.add("third word is incorrect,it should be -CarryingCapacity-");
        }
        try {
            if (Integer.parseInt(arrayOfStr[3]) < 0) {
                list.add("carryingCap cant be less than 0");
            }
        } catch (Exception e) {
            list.add("carryingCap cant be parse in int");
        }
        if (!arrayOfStr[4].equalsIgnoreCase("holdingCapacity")) {
            list.add("fifth word is incorrect,it should be -holdingCapacity-");
        }
        try {
            if (Integer.parseInt(arrayOfStr[5]) < 0) {
                list.add("holdingCap cant be less than 0");
            }
        } catch (Exception e) {
            list.add("holdingCap cant be parse in int");
        }
        if (!arrayOfStr[6].equalsIgnoreCase("cost")) {
            list.add("seventh word is incorrect, it should be -cost-");
        }
        try {
            if (Integer.parseInt(arrayOfStr[7]) < 0) {
                list.add("cost cant be less than 0");
            }
        } catch (Exception e) {
            list.add("cost cant be parse in int");
        }
        if (!arrayOfStr[8].equalsIgnoreCase("carModel")) {
            list.add("eighth word is incorrect, it should be -carModel-");
        }
        try {
            CarModel.valueOf(arrayOfStr[9].toUpperCase());
        } catch (Exception e) {
            list.add("incorrect Model of car");
        }

        if (!list.isEmpty()) {
            map.put("problem with line :" + counter, list);
        }

        return map;
    }
}
