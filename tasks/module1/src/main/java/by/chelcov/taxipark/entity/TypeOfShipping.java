package by.chelcov.taxipark.entity;

public enum TypeOfShipping {
    OFFICE_SHIPPING,
    SHIPPING_OF_HEAVY_LOAD,
    SHIPPING_IN_THE_CITY
}
