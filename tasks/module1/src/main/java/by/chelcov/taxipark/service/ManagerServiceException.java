package by.chelcov.taxipark.service;

public class ManagerServiceException extends Exception {
    public ManagerServiceException() {
    }

    public ManagerServiceException(String message) {
        super(message);
    }

    public ManagerServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ManagerServiceException(Throwable cause) {
        super(cause);
    }
}
