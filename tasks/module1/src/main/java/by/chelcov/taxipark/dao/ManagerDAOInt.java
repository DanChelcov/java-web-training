package by.chelcov.taxipark.dao;


import by.chelcov.taxipark.entity.Car;
import by.chelcov.taxipark.service.ManagerServiceException;

import java.util.List;

public interface ManagerDAOInt {
    long scoreCost();

    List<Car> findOnDiapasonOfCarrying(int minCarryingCapacity, int maxCarryingCapacity) throws ManagerServiceException;

    List<Car> findOnDiapasonOfHolding(int minHoldingCapacity, int maxHoldingCapacity) throws ManagerServiceException;

    List<Car> findOnDiapasonOfCarryingAndHolding(int minCarryingCapacity, int maxCarryingCapacity, int minHoldingCapacity, int maxHoldingCapacity) throws ManagerServiceException;

}
