package by.chelcov.taxipark.controller;

public class ProblemWithFileException extends Exception {
    public ProblemWithFileException() {
    }

    public ProblemWithFileException(String message) {
        super(message);
    }

    public ProblemWithFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProblemWithFileException(Throwable cause) {
        super(cause);
    }
}
