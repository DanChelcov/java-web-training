package by.chelcov.taxipark.service;

import by.chelcov.taxipark.controller.ControllerException;
import by.chelcov.taxipark.entity.Car;

public interface CarServiceInt {
    void addCar(Car car) throws ControllerException;

    void removeCar(Car car) throws ControllerException;
}
