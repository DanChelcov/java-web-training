package by.chelcov.taxipark.entity;

public enum TypeOfTaxi {
    ECONOM,
    BUSINESS,
    FAMILY
}
