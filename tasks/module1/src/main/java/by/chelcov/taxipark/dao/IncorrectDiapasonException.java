package by.chelcov.taxipark.dao;

public class IncorrectDiapasonException extends Exception {
    public IncorrectDiapasonException() {
    }

    public IncorrectDiapasonException(String message) {
        super(message);
    }

    public IncorrectDiapasonException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectDiapasonException(Throwable cause) {
        super(cause);
    }
}
