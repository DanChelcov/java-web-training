package by.chelcov.taxipark.controller;

import by.chelcov.taxipark.entity.CarType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InfoValidator implements Validate {

    public Map<String, List<String>> faultInfo;

    private InfoValidator() {
        faultInfo = new HashMap<>();
    }

    public static InfoValidator checkForInfoValidation(String line, int counter) {
        InfoValidator infoValidator = new InfoValidator();
        if (!new LineValidator(line).isValid()) {
            List<String> list = new ArrayList<>();
            list.add("Wrong amount of words");
            infoValidator.faultInfo.put("problem line :" + counter, list);
        } else {

            line = line.replace(" ", ",");
            line = line.replace(":", ",");
            String[] arrayOfStr = line.split("[,]+");

            switch (CarType.valueOf(arrayOfStr[1].toUpperCase())) {
                case TAXI:
                    infoValidator.faultInfo.putAll(new TaxiValidator().ValidateType(arrayOfStr, counter));
                    break;

                case MINIBUS:
                    infoValidator.faultInfo.putAll(new MinibusValidator().ValidateType(arrayOfStr, counter));
                    break;
                case FREIGHT_TAXI:
                    infoValidator.faultInfo.putAll(new FreightTaxiValidator().ValidateType(arrayOfStr, counter));
                    break;
                default:
                    List<String> list = new ArrayList<>();
                    list.add("Information is not correct");
                    list.add("Type-word is wrong");
                    infoValidator.faultInfo.put("problem line :" + counter, list);
            }
        }
        return infoValidator;
    }


    @Override
    public boolean isValid() {
        return faultInfo.isEmpty();
    }
}
