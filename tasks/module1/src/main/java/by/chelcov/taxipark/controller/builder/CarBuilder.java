package by.chelcov.taxipark.controller.builder;

import by.chelcov.taxipark.entity.Car;
import by.chelcov.taxipark.entity.CarCreatorException;

public interface CarBuilder {
    Car buildCar () throws CarCreatorException;
}
