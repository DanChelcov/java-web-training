package by.chelcov.taxipark.entity;



abstract public class Car {
    CarType carType;
    private int carryingCapacity;
    private int holdingCapacity;
    private int cost;
    private CarModel model;


    public Car(int carryingCapacity, int holdingCapacity, int cost, CarModel model) throws CarCreatorException {
        if(carryingCapacity <= 0 || holdingCapacity <= 0 || cost  < 0) throw new CarCreatorException("incorrect parameters");
        this.cost = cost;
        this.carryingCapacity = carryingCapacity;
        this.holdingCapacity = holdingCapacity;
        this.model = model;
    }

    public CarType getCarType() {
        return carType;
    }


    public int getCarryingCapacity() {
        return carryingCapacity;
    }

    public void setCarryingCapacity(int carryingCapacity)throws CarCreatorException{
        if(carryingCapacity <= 0) throw new CarCreatorException("carryingCapacity cant be less than 0");
        this.carryingCapacity = carryingCapacity;
    }

    public int getHoldingCapacity() {
        return holdingCapacity;
    }

    public void setHoldingCapacity(int holdingCapacity)throws CarCreatorException  {
        if(holdingCapacity <= 0) throw new CarCreatorException("holdingCapacity cant be less than 0");
        this.holdingCapacity = holdingCapacity;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }


    public CarModel getModel() {
        return model;
    }

    public void setModel(CarModel model)throws CarCreatorException {
        if(cost <= 0) throw new CarCreatorException("cost cant be less than 0");
        this.model = model;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return carryingCapacity == car.carryingCapacity &&
                holdingCapacity == car.holdingCapacity &&
                cost == car.cost &&
                carType.equals(car.carType) &&
                model.equals(car.model);
    }

    @Override
    public int hashCode() {
        int result;
        return result = 31 + carryingCapacity + holdingCapacity + cost + model.hashCode() + carType.hashCode();
    }

    @Override
    public String toString() {
        return "carryingCapacity = " + carryingCapacity +
                ", carType = " + carType +
                ", holdingCapacity = " + holdingCapacity +
                ", cost = " + cost +
                ", model = " + model.toString();
    }
}
