package by.chelcov.taxipark.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExcWorker {
    public static void workWithExc(Exception e, Map<String, List<String>> informationAboutErr) {
        List<String> list = new ArrayList<>();
        list.add(e.getMessage());
        list.add(String.valueOf(e.hashCode()));
        informationAboutErr.put(e.getClass().getName(), list);
    }
}
