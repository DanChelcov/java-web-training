package by.chelcov.taxipark.entity;

public enum TypeOfMinibus {
    PASSENGER,
    CARGO,
    HYBRID
}
