package by.chelcov.taxipark.service;

import by.chelcov.taxipark.controller.ControllerException;
import by.chelcov.taxipark.entity.Car;

import java.util.List;

public interface ManagerServiceInt {
    List<Car> findOnDiapasonOfCarrying(int minCarryingCapacity, int maxCarryingCapacity) throws ControllerException;

    List<Car> findOnDiapasonOfHolding(int minHoldingCapacity, int maxHoldingCapacity) throws ControllerException;

    List<Car> findOnDiapasonOfCarryingAndHolding(int minCarryingCapacity, int maxCarryingCapacity, int minHoldingCapacity, int maxHoldingCapacity) throws ControllerException;

}
