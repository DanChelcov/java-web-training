package by.chelcov.taxipark.dao;

import by.chelcov.taxipark.entity.*;
import by.chelcov.taxipark.service.CarServiceException;
import by.chelcov.taxipark.service.ManagerServiceException;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class DAOTaxipark implements CarDAOInt, ManagerDAOInt, SortDAOInt {

    private List<Car> taxipark = new ArrayList<Car>();


    public DAOTaxipark() throws CarCreatorException {

        taxipark.add(new Taxi(124, 3, 190, CarModel.BUICK, TypeOfTaxi.BUSINESS));
        taxipark.add(new FreightTaxi(6780, 32, 5900, CarModel.RENAULT, TypeOfShipping.OFFICE_SHIPPING));
        taxipark.add(new Taxi(984, 6, 4900, CarModel.FORD, TypeOfTaxi.FAMILY));
        taxipark.add(new Minibus(1240, 30, 3200, CarModel.MAZDA, TypeOfMinibus.PASSENGER));

    }

    public void addCar(Car car) throws CarServiceException {
        try {
            if (car == null) throw new CarMoveException("car is null");
            taxipark.add(car);
        } catch (CarMoveException e) {
          throw  new CarServiceException(e);
        }
    }

    public void removeCar(Car car) throws CarServiceException  {
        try {
            if (car == null) throw new CarMoveException("car is null");
            if (!taxipark.contains(car)) throw new CarMoveException("taxiPark doesn`t have this car");
            taxipark.remove(car);
        } catch (CarMoveException e) {
           throw  new CarServiceException(e);
        }


    }

    public long scoreCost() {
        int cost = 0;
        for (Car car : taxipark) {
            cost += car.getCost();
        }
        return cost;
    }

    public List<Car> findOnDiapasonOfCarrying(int minCarryingCapacity, int maxCarryingCapacity)throws ManagerServiceException {
        List<Car> newList = new ArrayList<Car>();
        try {
            if (minCarryingCapacity < 0 || maxCarryingCapacity <= minCarryingCapacity )
                throw new IncorrectDiapasonException("incorrect bounds ");
            for (Car car : taxipark) {
                if (car.getCarryingCapacity() >= minCarryingCapacity && car.getCarryingCapacity() <= maxCarryingCapacity) {
                    newList.add(car);
                }

            }

        } catch (IncorrectDiapasonException e) {
           throw  new ManagerServiceException(e);

        }
        return newList;
    }

    public List<Car> findOnDiapasonOfHolding(int minHoldingCapacity, int maxHoldingCapacity) throws ManagerServiceException {
        List<Car> newList = new ArrayList<Car>();
        try {
            if (minHoldingCapacity < 0 || maxHoldingCapacity <= minHoldingCapacity )
                throw new IncorrectDiapasonException("incorrect bounds ");
            for (Car car : taxipark) {
                if (car.getHoldingCapacity() >= minHoldingCapacity && car.getHoldingCapacity() <= maxHoldingCapacity) {
                    newList.add(car);
                }
            }

        } catch (IncorrectDiapasonException e) {
           throw  new ManagerServiceException(e);


        }
        return newList;
    }

    public List<Car> findOnDiapasonOfCarryingAndHolding(int minCarryingCapacity, int maxCarryingCapacity,
                                                        int minHoldingCapacity, int maxHoldingCapacity) throws ManagerServiceException {
        List<Car> newList = new ArrayList<Car>();
        try {
            if (minHoldingCapacity < 0 || maxHoldingCapacity <= minHoldingCapacity  ||
                    minCarryingCapacity < 0 || maxCarryingCapacity <= minCarryingCapacity )
                throw new IncorrectDiapasonException("incorrect bounds ");
            for (Car car : taxipark) {
                if (car.getCarryingCapacity() >= minCarryingCapacity && car.getCarryingCapacity() <= maxCarryingCapacity &&
                        car.getHoldingCapacity() >= minHoldingCapacity && car.getHoldingCapacity() <= maxHoldingCapacity) {
                    newList.add(car);
                }
            }
        }catch (IncorrectDiapasonException e){
           throw  new ManagerServiceException(e);
        }
        return newList;
    }

    public void sortByCarryingCap() {
        Collections.sort(this.taxipark, new CarComparator.CarryingComp().thenComparing(new CarComparator.HoldingComp()).thenComparing(new CarComparator.CostComp()));

    }

    public void sortByHoldingCap() {
        Collections.sort(this.taxipark, new CarComparator.HoldingComp().thenComparing(new CarComparator.CostComp()).thenComparing(new CarComparator.CarryingComp()));
    }

    public void sortByCost() {
        Collections.sort(this.taxipark, new CarComparator.CostComp().thenComparing(new CarComparator.CarryingComp()).thenComparing(new CarComparator.HoldingComp()));
    }


    public List<Car> getListCar() {
        return taxipark;
    }

}
