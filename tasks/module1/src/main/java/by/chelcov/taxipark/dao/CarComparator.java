package by.chelcov.taxipark.dao;

import by.chelcov.taxipark.entity.Car;

import java.util.Comparator;

public class CarComparator {
    public static class CarryingComp implements Comparator<Car>{

        @Override
        public  int compare(Car o1, Car o2) {
            return Integer.compare(o2.getCarryingCapacity(), o1.getCarryingCapacity());
        }
    }
    public static class HoldingComp implements Comparator<Car>{

        @Override
        public  int compare(Car o1, Car o2) {
            return Integer.compare(o2.getHoldingCapacity(), o1.getHoldingCapacity());
        }
    }
    public static class CostComp implements Comparator<Car>{

        @Override
        public  int compare(Car o1, Car o2) {
            return Integer.compare(o2.getCost(), o1.getCost());
        }
    }
}
