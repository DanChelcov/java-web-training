package by.chelcov.taxipark.dao;

public class CarMoveException extends Exception {
    public CarMoveException() {
    }

    public CarMoveException(String message) {
        super(message);
    }

    public CarMoveException(String message, Throwable cause) {
        super(message, cause);
    }

    public CarMoveException(Throwable cause) {
        super(cause);
    }
}
