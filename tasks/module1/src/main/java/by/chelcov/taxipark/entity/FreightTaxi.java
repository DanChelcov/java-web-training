package by.chelcov.taxipark.entity;



public class FreightTaxi extends Car {
    private TypeOfShipping type;


    public FreightTaxi(int carryingCapacity, int holdingCapacity, int cost, CarModel model, TypeOfShipping type)
            throws CarCreatorException {
        super(carryingCapacity, holdingCapacity,cost, model);
        carType = CarType.FREIGHT_TAXI;
        this.type = type;
    }

    public TypeOfShipping getType() {
        return type;
    }

    public void setType(TypeOfShipping type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        FreightTaxi that = (FreightTaxi) o;
        return type.equals(that.type);
    }
    @Override
    public int hashCode() {
        int result;
        return result = 31 + super.hashCode() + type.hashCode();
    }

    @Override
    public String toString() {
        return "FreightTaxi{" +
                "type = " + type + ", " +
                super.toString() +
                '}';
    }
}
