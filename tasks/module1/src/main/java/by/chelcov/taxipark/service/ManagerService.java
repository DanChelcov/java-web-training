package by.chelcov.taxipark.service;

import by.chelcov.taxipark.controller.ControllerException;
import by.chelcov.taxipark.dao.DAOTaxipark;
import by.chelcov.taxipark.dao.ManagerDAOInt;
import by.chelcov.taxipark.entity.Car;

import java.util.ArrayList;
import java.util.List;

public class ManagerService implements ManagerServiceInt {
    private DAOTaxipark daoTaxipark;

    public ManagerService(DAOTaxipark daoTaxipark) {
        this.daoTaxipark = daoTaxipark;
    }

    public List<Car> findOnDiapasonOfCarrying(int minCarryingCapacity, int maxCarryingCapacity) throws ControllerException {
        try {
            return daoTaxipark.findOnDiapasonOfCarrying(minCarryingCapacity, maxCarryingCapacity);
        } catch (ManagerServiceException e) {
            throw new ControllerException(e);
        }
    }

    public List<Car> findOnDiapasonOfHolding(int minHoldingCapacity, int maxHoldingCapacity) throws ControllerException {
        try {
            return daoTaxipark.findOnDiapasonOfHolding(minHoldingCapacity, maxHoldingCapacity);
        } catch (ManagerServiceException e) {
            throw new ControllerException(e);
        }
    }

    public List<Car> findOnDiapasonOfCarryingAndHolding(int minCarryingCapacity, int maxCarryingCapacity,
                                                        int minHoldingCapacity, int maxHoldingCapacity) throws ControllerException {
        try {
            return daoTaxipark.findOnDiapasonOfCarryingAndHolding(minCarryingCapacity, maxCarryingCapacity,
                    minHoldingCapacity, maxHoldingCapacity);
        } catch (ManagerServiceException e) {
            throw new ControllerException(e);
        }
    }
}
