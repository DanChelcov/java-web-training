package by.chelcov.taxipark.controller;

import by.chelcov.taxipark.entity.TypeOfTaxi;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TaxiValidator implements CarTypeValidate {
    @Override
    public Map<String, List<String>> ValidateType(String[] arrayOfStr, int counter) {
        Map<String, List<String>> map = new CarValidator().validateCar(arrayOfStr, counter);
        List<String> list = new ArrayList<>();
        if (!arrayOfStr[10].equalsIgnoreCase("TypeOfTaxi")) {
            list.add("tenth word is incorrect,it should be -TypeOfTaxi-");
        }
        try {
            TypeOfTaxi.valueOf(arrayOfStr[11].toUpperCase());
        } catch (Exception e) {
            list.add("incorrect TypeOfTaxi of car");
        }
        if (list.isEmpty()) {
            return map;
        }

        map.put("problem with line :" + counter, list);


        return map;
    }
}
