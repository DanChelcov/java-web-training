package by.chelcov.taxipark.entity;



public class Taxi extends Car {
    private TypeOfTaxi type;

    public Taxi(int carryingCapacity, int holdingCapacity, int cost, CarModel model, TypeOfTaxi type) throws CarCreatorException {
        super(carryingCapacity, holdingCapacity, cost, model);
        carType = CarType.TAXI;
        this.type = type;
    }

    public TypeOfTaxi getType() {
        return type;
    }

    public void setType(TypeOfTaxi type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Taxi taxi = (Taxi) o;
        return type.equals(taxi.type);
    }

    @Override
    public int hashCode() {
        int result;
        return result = 31 + super.hashCode() + type.hashCode();
    }

    @Override
    public String toString() {
        return "Taxi{" +
                "type = " + type +", " +
                super.toString() +
                '}';
    }
}
