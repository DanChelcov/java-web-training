package by.training.entity;

import java.util.Objects;

public class WordEntity {
    private String value;
    private long id;
    private long sentenceId;
    private int order;

    public WordEntity(String value){
        this.value = value;
    }

    public WordEntity(String value, long sentenceId) {
        this.value = value;
        this.sentenceId = sentenceId;
    }
    public WordEntity(String value, long sentenceId, int order) {
        this.value = value;
        this.sentenceId = sentenceId;
        this.order = order;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSentenceId() {
        return sentenceId;
    }

    public void setSentenceId(long sentenceId) {
        this.sentenceId = sentenceId;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WordEntity that = (WordEntity) o;
        return id == that.id &&
                sentenceId == that.sentenceId &&
                order == that.order &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, id, sentenceId, order);
    }

    @Override
    public String toString() {
        return "WordEntity{" +
                "value='" + value + '\'' +
                ", wordId=" + id +
                ", sentenceId=" + sentenceId +
                ", order=" + order +
                '}';
    }
}
