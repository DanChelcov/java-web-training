package by.training.entity;

import java.util.Objects;

public class SentenceEntity {
    private long id;
    private long paragraphId;
    private int order;

    public SentenceEntity() {
    }

    public SentenceEntity(long paragraphId) {
        this.paragraphId = paragraphId;

    }
    public SentenceEntity(long paragraphId, int order) {
        this.paragraphId = paragraphId;
        this.order = order;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getParagraphId() {
        return paragraphId;
    }

    public void setParagraphId(long paragraphId) {
        this.paragraphId = paragraphId;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SentenceEntity that = (SentenceEntity) o;
        return id == that.id &&
                paragraphId == that.paragraphId &&
                order == that.order;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, paragraphId, order);
    }

    @Override
    public String toString() {
        return "SentenceEntity{" +
                "id=" + id +
                ", paragraphId=" + paragraphId +
                ", order=" + order +
                '}';
    }
}
