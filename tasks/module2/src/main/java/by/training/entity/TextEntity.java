package by.training.entity;

import java.util.Objects;

public class TextEntity {
    private long id;

    public TextEntity() {
    }

    public TextEntity(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TextEntity that = (TextEntity) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "TextEntity{" +
                "id=" + id +
                '}';
    }
}
