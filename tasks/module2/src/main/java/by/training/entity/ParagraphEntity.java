package by.training.entity;

import java.util.Objects;

public class ParagraphEntity {
    private long id;
    private long textId;
    private int order;

    public ParagraphEntity() {
    }

    public ParagraphEntity(long textId) {
        this.textId = textId;
    }

    public ParagraphEntity(long textId, int order) {
        this.textId = textId;
        this.order = order;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTextId() {
        return textId;
    }

    public void setTextId(long textId) {
        this.textId = textId;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParagraphEntity that = (ParagraphEntity) o;
        return id == that.id &&
                textId == that.textId &&
                order == that.order;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, textId, order);
    }

    @Override
    public String toString() {
        return "ParagraphEntity{" +
                "id=" + id +
                ", textId=" + textId +
                ", order=" + order +
                '}';
    }
}
