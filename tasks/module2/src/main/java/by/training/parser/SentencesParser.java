package by.training.parser;

import by.training.model.LeafString;
import by.training.model.SentenceLeaf;
import by.training.util.StringWorker;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SentencesParser extends StringParser {
    public static final String SENTENCE_REGEX = ("([A-Z]{1})(.)+?(\\.)");

    public SentencesParser(StringProcessor next) {
        super(next);
    }

    @Override
    public List<LeafString> processString(String string) {
        if (next == null) {
            return new ArrayList<>();
        }
        List<String> stringList = StringWorker.fromStringToList(string, SENTENCE_REGEX);
        return stringList.stream().map(this::apply).collect(Collectors.toList());

    }

    private SentenceLeaf apply(String s) {
        SentenceLeaf sentenceLeaf = new SentenceLeaf();
        sentenceLeaf.addListOfSubLeafs(next.processString(s));
        return sentenceLeaf;
    }
}
