package by.training.parser;

import by.training.model.LeafString;

import java.util.List;

public interface StringProcessor<T extends LeafString> {
    List<T> processString(String stringBuilder);

    void setNext(StringProcessor<T> stringProcessor);
}
