package by.training.parser;

import by.training.model.LeafString;
import by.training.model.WordLeaf;
import by.training.util.StringWorker;

import java.util.List;
import java.util.stream.Collectors;

public class WordsParser extends StringParser {
    public static final String SENTENCE_REGEX = "([\\'\\(]+)?([\\w\\-]+)([\\.\\,\\'\\)\\:]+)?";

    @Override
    public List<LeafString> processString(String string) {
        List<String> stringList = StringWorker.fromStringToList(string, SENTENCE_REGEX);
        return stringList.stream().map(WordLeaf::new).collect(Collectors.toList());

    }
}
