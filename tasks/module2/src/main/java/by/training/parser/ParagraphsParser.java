package by.training.parser;

import by.training.model.LeafString;
import by.training.model.ParagraphLeaf;
import by.training.util.StringWorker;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ParagraphsParser extends StringParser {
    public static final String PARAGRAPH_REGEX = "(\t|(( ){4,5}))+(.)+(\n)?";

    public ParagraphsParser(StringProcessor next) {
        super(next);
    }

    @Override
    public List<LeafString> processString(String string) {
        if (next == null) {
            return new ArrayList<>();
        }
        List<String> stringList = StringWorker.fromStringToList(string, PARAGRAPH_REGEX);
        return stringList.stream().map(this::apply).collect(Collectors.toList());

    }

    private ParagraphLeaf apply(String s) {
        ParagraphLeaf paragraphLeaf = new ParagraphLeaf();
        paragraphLeaf.addListOfSubLeafs(next.processString(s));
        return paragraphLeaf;
    }
}
