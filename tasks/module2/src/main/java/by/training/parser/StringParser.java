package by.training.parser;


public abstract class StringParser implements StringProcessor {
    protected StringProcessor next;

    protected StringParser() {
    }

    protected StringParser(StringProcessor next) {
        this.next = next;
    }


    @Override
    public void setNext(StringProcessor stringProcessor) {
        next = stringProcessor;
    }
}
