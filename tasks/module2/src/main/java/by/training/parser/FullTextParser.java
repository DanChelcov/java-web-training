package by.training.parser;


import by.training.model.TextLeaf;

public class FullTextParser {
    private final StringParser paragraphsParser, sentencesParser, wordParser;


    public FullTextParser() {
        wordParser = new WordsParser();
        sentencesParser = new SentencesParser(wordParser);
        paragraphsParser = new ParagraphsParser(sentencesParser);
    }

    public TextLeaf getTextLeaf(String text) {
        TextLeaf textLeaf = new TextLeaf();
        textLeaf.addListOfSubLeafs(paragraphsParser.processString(text));
        return textLeaf;
    }
}
