package by.training.comparator;

import by.training.model.LeafString;
import by.training.model.SentenceLeaf;

import java.util.Comparator;

public class ParagraphSortSentencesLengthComparator implements Comparator<LeafString> {
    private boolean reverse;

    public ParagraphSortSentencesLengthComparator() {
    }

    public ParagraphSortSentencesLengthComparator(boolean reverse) {
        this.reverse = reverse;
    }

    public boolean isReverse() {
        return reverse;
    }

    public void setReverse(boolean reverse) {
        this.reverse = reverse;
    }

    @Override
    public int compare(LeafString sentenceLeaf1, LeafString sentenceLeaf2) {
        if(reverse){
            return Integer.compare(sentenceLeaf1.getNumberOfElements(), sentenceLeaf2.getNumberOfElements());
        }else {
            return Integer.compare(sentenceLeaf2.getNumberOfElements(), sentenceLeaf1.getNumberOfElements());
        }
    }
}
