package by.training.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringWorker {
    public static List<String> fromStringToList(String str, String regex){
        List<String> list = new ArrayList<>();
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        while (matcher.find()){
            list.add(matcher.group());
        }
       return list;
    }

}
