package by.training.model;

import java.util.LinkedList;
import java.util.List;

public class ParagraphLeaf implements CompositeString {
    private final List<LeafString> listOfSentences;

    public ParagraphLeaf() {
        this.listOfSentences = new LinkedList<>();
    }

    @Override
    public void addSubLeaf(LeafString leafString) {
        listOfSentences.add(leafString);

    }

    public void addListOfSubLeafs(List<LeafString> list) {
        list.stream().forEach(this::addSubLeaf);
    }

    @Override
    public void removeStrings(LeafString leafString) {
        listOfSentences.remove(leafString);
    }

    public List<LeafString> getListOfComponents() {
        return listOfSentences;
    }

    @Override
    public String getStrings() {
        StringBuilder stringBuilder = new StringBuilder();

        listOfSentences.stream().forEach(s -> stringBuilder.append(s.getStrings()));
        return "\t" + stringBuilder.toString().trim() + "\n";
    }

    @Override
    public int getNumberOfElements() {
        return listOfSentences.size();
    }
}
