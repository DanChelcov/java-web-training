package by.training.model;

public interface LeafString {
    String getStrings();

    int getNumberOfElements();
}
