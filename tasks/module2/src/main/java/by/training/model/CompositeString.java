package by.training.model;

import java.util.List;

public interface CompositeString extends LeafString {
    void addSubLeaf(LeafString leafString);

    void removeStrings(LeafString leafString);

    List<LeafString> getListOfComponents();
}
