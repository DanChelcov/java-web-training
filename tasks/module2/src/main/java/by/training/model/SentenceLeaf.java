package by.training.model;

import java.util.LinkedList;
import java.util.List;

public class SentenceLeaf implements CompositeString {
    private final List<LeafString> listOfWords;

    public SentenceLeaf() {
        this.listOfWords = new LinkedList<>();
    }

    public void addSubLeaf(LeafString leafString) {
        listOfWords.add(leafString);

    }

    public void addListOfSubLeafs(List<LeafString> list) {
        list.stream().forEach(this::addSubLeaf);
    }

    public void removeStrings(LeafString leafString) {
        listOfWords.remove(leafString);

    }

    public List<LeafString> getListOfComponents() {
        return listOfWords;
    }

    public String getStrings() {
        StringBuilder stringBuilder = new StringBuilder();

        listOfWords.stream().forEach(s -> stringBuilder.append(s.getStrings()));
        return stringBuilder.toString();
    }

    @Override
    public int getNumberOfElements() {
        return listOfWords.size();
    }


}
