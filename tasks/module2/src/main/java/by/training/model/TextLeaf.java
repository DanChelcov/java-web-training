package by.training.model;

import java.util.LinkedList;
import java.util.List;

public class TextLeaf implements CompositeString {
    private final List<LeafString> listOfParagraphs;

    public TextLeaf() {
        this.listOfParagraphs = new LinkedList<>();
    }

    @Override
    public void addSubLeaf(LeafString leafString) {
        listOfParagraphs.add(leafString);
    }

    public void addListOfSubLeafs(List<LeafString> list) {
        list.stream().forEach(this::addSubLeaf);
    }

    @Override
    public void removeStrings(LeafString leafString) {
        listOfParagraphs.remove(leafString);

    }

    @Override
    public List<LeafString> getListOfComponents() {
        return listOfParagraphs;
    }

    @Override
    public String getStrings() {
        StringBuilder stringBuilder = new StringBuilder();
        listOfParagraphs.stream().forEach(s -> stringBuilder.append(s.getStrings()));
        return stringBuilder.toString();
    }


    @Override
    public int getNumberOfElements() {
        return listOfParagraphs.size();
    }
}
