package by.training.model;

public class WordLeaf implements LeafString {
    private final String word;

    public WordLeaf(String word) {
        this.word = word;
    }

    public String getStrings() {
        return new StringBuilder(word + " ").toString();
    }

    @Override
    public int getNumberOfElements() {
        return word.length();
    }
}
