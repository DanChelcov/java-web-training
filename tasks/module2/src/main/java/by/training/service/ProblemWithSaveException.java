package by.training.service;

public class ProblemWithSaveException extends Exception {
    public Exception exception;

    public ProblemWithSaveException() {

    }

    public ProblemWithSaveException(String message) {
        super(message);
    }

    public ProblemWithSaveException(String message, Exception exception) {
        super(message);
        this.exception = exception;
    }
}
