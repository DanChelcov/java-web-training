package by.training.service;

public class SortException extends Exception {
    public Exception exception;

    public SortException() {

    }

    public SortException(String message) {
        super(message);
    }

    public SortException(String message, Exception exception) {
        super(message);
        this.exception = exception;
    }
}
