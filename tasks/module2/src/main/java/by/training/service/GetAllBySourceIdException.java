package by.training.service;

public class GetAllBySourceIdException extends Exception {
    public Exception exception;

    public GetAllBySourceIdException() {

    }

    public GetAllBySourceIdException(String message) {
        super(message);
    }

    public GetAllBySourceIdException(String message, Exception exception) {
        super(message);
        this.exception = exception;
    }

    public GetAllBySourceIdException(Exception exception) {
        this.exception = exception;
    }
}
