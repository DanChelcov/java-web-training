package by.training.service;

public class ModelGettingException extends Exception {
    public Exception exception;

    public ModelGettingException() {

    }

    public ModelGettingException(String message) {
        super(message);
    }

    public ModelGettingException(String message, Exception exception) {
        super(message);
        this.exception = exception;
    }
}
