package by.training.service;

import by.training.entity.SentenceEntity;
import by.training.entity.WordEntity;
import by.training.model.LeafString;
import by.training.model.SentenceLeaf;
import by.training.model.WordLeaf;
import by.training.repository.CreatorException;
import by.training.repository.IncorrectIdException;
import by.training.repository.SentenceRepository;
import by.training.repository.WordRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class SentenceService {
    private final SentenceRepository sentenceRepository;
    private final WordRepository wordRepository;

    public SentenceService(SentenceRepository sentenceRepository, WordRepository wordRepository) {
        this.sentenceRepository = sentenceRepository;
        this.wordRepository = wordRepository;
    }

    public long save(SentenceLeaf sentenceLeaf) throws ProblemWithSaveException {
        try {
            long sentenceId = sentenceRepository.create(new SentenceEntity());
            int wordOrder = 1;
            for (LeafString leafStringWord : sentenceLeaf.getListOfComponents()) {
                WordLeaf wordLeaf = (WordLeaf) leafStringWord;
                wordRepository.create(new WordEntity(wordLeaf.getStrings(), sentenceId, wordOrder++));
            }
            return sentenceId;
        } catch (CreatorException e) {
            throw new ProblemWithSaveException("Sentence cannot be saved. Some details", e);
        }
    }

    public SentenceLeaf getSentenceModel(long id) throws ModelGettingException {
        try {
            if (sentenceRepository.getAllItems().stream().anyMatch(sentenceEntity -> sentenceEntity.getId() == id)) {
                SentenceLeaf sentenceLeaf = new SentenceLeaf();
                List<WordEntity> wordEntityList = wordRepository.getAllItemsBySourceId(id);
                for (WordEntity wordEntity : wordEntityList) {
                    WordLeaf wordLeaf = new WordLeaf(wordEntity.getValue());
                    sentenceLeaf.addSubLeaf(wordLeaf);
                }
                return sentenceLeaf;
            } else {
                throw new ModelGettingException("repository doesnt have text with this id");
            }
        } catch (IncorrectIdException e) {
            throw new ModelGettingException("cant get sentence model from rep", e);
        }
    }

    public SentenceEntity readById(long id) throws CantReadException {
        try {
            return sentenceRepository.read(id);
        } catch (IncorrectIdException e) {
            throw new CantReadException("cant read sentence from rep", e);
        }
    }

    public List<SentenceEntity> getAllBySourceId(long id) throws GetAllBySourceIdException {
        try {
            return sentenceRepository.getAllItemsBySourceId(id);
        } catch (IncorrectIdException e) {
            throw new GetAllBySourceIdException(e);
        }
    }

    public List<Long> getIdList() {

        return sentenceRepository.getListOfId();
    }

    public List<SentenceEntity> getAllSentences() {
        return new LinkedList<>(sentenceRepository.getAllItems());
    }

    public boolean deleteSentence(long id) {
        return sentenceRepository.delete(id);
    }

    public List<SentenceEntity> sort(Comparator<SentenceEntity> comparator) throws SortException {
        if (comparator == null) throw new SortException("cant sort sentences entities : comparator is null");
        List<SentenceEntity> sentenceEntityList = new ArrayList<>(sentenceRepository.getAllItems());
        sentenceEntityList.sort(comparator);
        return sentenceEntityList;
    }

    public SentenceLeaf getSortedSentenceModel(Comparator<LeafString> comparator, long id) throws SortException, ModelGettingException {
        if (comparator == null) throw new SortException("comparator is null");
        SentenceLeaf sentenceLeaf = getSentenceModel(id);
        sentenceLeaf.getListOfComponents().sort(comparator);
        return sentenceLeaf;
    }
}
