package by.training.service;

import by.training.entity.ParagraphEntity;
import by.training.entity.SentenceEntity;
import by.training.entity.TextEntity;
import by.training.entity.WordEntity;
import by.training.model.LeafString;
import by.training.model.ParagraphLeaf;
import by.training.model.SentenceLeaf;
import by.training.model.TextLeaf;
import by.training.model.WordLeaf;
import by.training.repository.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class TextService {
    private final TextRepository textRepository;
    private final ParagraphRepository paragraphRepository;
    private final SentenceRepository sentenceRepository;
    private final WordRepository wordRepository;

    public TextService(TextRepository textRepository, ParagraphRepository paragraphRepository,
                       SentenceRepository sentenceRepository, WordRepository wordRepository) {
        this.textRepository = textRepository;
        this.paragraphRepository = paragraphRepository;
        this.sentenceRepository = sentenceRepository;
        this.wordRepository = wordRepository;
    }


    public long save(TextLeaf textLeaf) throws ProblemWithSaveException {
        try {
            long textId = textRepository.create(new TextEntity());
            int parOrder = 1;
            for (LeafString leafStringPar : textLeaf.getListOfComponents()) {
                ParagraphLeaf paragraphLeaf = (ParagraphLeaf) leafStringPar;
                long paragraphId = paragraphRepository.create(new ParagraphEntity(textId, parOrder++));
                int sentOrder = 1;
                for (LeafString leafStringSent : paragraphLeaf.getListOfComponents()) {
                    SentenceLeaf sentenceLeaf = (SentenceLeaf) leafStringSent;
                    long sentenceId = sentenceRepository.create(new SentenceEntity(paragraphId, sentOrder++));
                    int wordOrder = 1;
                    for (LeafString leafStringWord : sentenceLeaf.getListOfComponents()) {
                        WordLeaf wordLeaf = (WordLeaf) leafStringWord;
                        wordRepository.create(new WordEntity(wordLeaf.getStrings(), sentenceId, wordOrder));
                    }
                }
            }
            return textId;
        } catch (CreatorException e) {
            throw new ProblemWithSaveException("Text cannot be saved. Some details", e);
        }
    }

    public TextLeaf getTextModel(long id) throws ModelGettingException {
        try {
            if (textRepository.getAllItems().stream().anyMatch(textEntity -> textEntity.getId() == id)) {
                TextLeaf textLeaf = new TextLeaf();
                List<ParagraphEntity> paragraphEntityList = paragraphRepository.getAllItemsBySourceId(id);
                for (ParagraphEntity paragraphEntity : paragraphEntityList) {
                    ParagraphLeaf paragraphLeaf = new ParagraphLeaf();
                    List<SentenceEntity> sentenceEntityList = sentenceRepository.getAllItemsBySourceId(paragraphEntity.getId());
                    for (SentenceEntity sentenceEntity : sentenceEntityList) {
                        SentenceLeaf sentenceLeaf = new SentenceLeaf();
                        List<WordEntity> wordEntityList = wordRepository.getAllItemsBySourceId(sentenceEntity.getId());
                        for (WordEntity wordEntity : wordEntityList) {
                            WordLeaf wordLeaf = new WordLeaf(wordEntity.getValue());
                            sentenceLeaf.addSubLeaf(wordLeaf);
                        }
                        paragraphLeaf.addSubLeaf(sentenceLeaf);
                    }
                    textLeaf.addSubLeaf(paragraphLeaf);
                }
                return textLeaf;
            } else {
                throw new ModelGettingException("repository doesnt have text with this id");
            }
        } catch (IncorrectIdException e) {
            throw new ModelGettingException("cant get text model from rep", e);
        }

    }

    public TextEntity readById(long id) throws CantReadException {
        try {
            return textRepository.read(id);
        } catch (IncorrectIdException e) {
            throw new CantReadException("cant read text from rep", e);
        }
    }

    public List<TextEntity> getAllBySourceId(long id) {
        return textRepository.getAllItemsBySourceId(id);
    }

    public List<Long> getIdList() {
        return textRepository.getListOfId();
    }

    public List<TextEntity> getAllTexts() {
        return new LinkedList<>(textRepository.getAllItems());
    }

    public boolean deleteText(long id) {
        return textRepository.delete(id);
    }

    public List<TextEntity> sort(Comparator<TextEntity> comparator) throws SortException {
        if (comparator == null) throw new SortException("cant sort sentences entities : comparator is null");
        List<TextEntity> textEntityList = new ArrayList<>(textRepository.getAllItems());
        textEntityList.sort(comparator);
        return textEntityList;
    }

    public TextLeaf getSortedTextModel(Comparator<LeafString> comparator, long id) throws SortException, ModelGettingException {
        if (comparator == null) throw new SortException("comparator is null");
        TextLeaf textLeaf = getTextModel(id);
        textLeaf.getListOfComponents().sort(comparator);
        return textLeaf;
    }

}
