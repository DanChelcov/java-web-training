package by.training.service;

import by.training.entity.WordEntity;
import by.training.model.WordLeaf;
import by.training.repository.CreatorException;
import by.training.repository.IncorrectIdException;
import by.training.repository.WordRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class WordService {
    private final WordRepository wordRepository;

    public WordService(WordRepository wordRepository) {
        this.wordRepository = wordRepository;
    }

    public long save(WordLeaf wordLeaf) throws ProblemWithSaveException {
        try {
            long wordId = wordRepository.create(new WordEntity(wordLeaf.getStrings()));
            return wordId;
        } catch (CreatorException e) {
            throw new ProblemWithSaveException("Word cannot be saved. Some details", e);
        }
    }

    public WordLeaf getWordModel(long id) throws ModelGettingException {
        try {
            return new WordLeaf(wordRepository.read(id).getValue());
        } catch (IncorrectIdException e) {
            throw new ModelGettingException("cant get word model from rep", e);
        }
    }

    public WordEntity readById(long id) throws CantReadException {
        try {
            return wordRepository.read(id);
        } catch (IncorrectIdException e) {
            throw new CantReadException("cant read word from rep", e);
        }
    }

    public List<WordEntity> getAllBySourceId(long id) throws GetAllBySourceIdException {
        try {
            return wordRepository.getAllItemsBySourceId(id);
        } catch (IncorrectIdException e) {
            throw new GetAllBySourceIdException(e);
        }
    }


    public List<Long> getIdList() {
        return wordRepository.getListOfId();
    }

    public List<WordEntity> getAllWords() {
        return new LinkedList<>(wordRepository.getAllItems());
    }

    public boolean deleteWord(long id) {
        return wordRepository.delete(id);
    }

    public List<WordEntity> sort(Comparator<WordEntity> comparator) throws SortException {
        if (comparator == null) throw new SortException("comparator is null");
        List<WordEntity> wordEntityList = new ArrayList<>(wordRepository.getAllItems());
        wordEntityList.sort(comparator);
        return wordEntityList;
    }
}
