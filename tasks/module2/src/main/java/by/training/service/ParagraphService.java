package by.training.service;

import by.training.entity.ParagraphEntity;
import by.training.entity.SentenceEntity;
import by.training.entity.WordEntity;
import by.training.model.LeafString;
import by.training.model.ParagraphLeaf;
import by.training.model.SentenceLeaf;
import by.training.model.WordLeaf;
import by.training.repository.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class ParagraphService {
    private final ParagraphRepository paragraphRepository;
    private final SentenceRepository sentenceRepository;
    private final WordRepository wordRepository;

    public ParagraphService(ParagraphRepository paragraphRepository, SentenceRepository sentenceRepository, WordRepository wordRepository) {
        this.paragraphRepository = paragraphRepository;
        this.sentenceRepository = sentenceRepository;
        this.wordRepository = wordRepository;
    }

    public long save(ParagraphLeaf paragraphLeaf) throws ProblemWithSaveException {
        try {
            long paragraphId = paragraphRepository.create(new ParagraphEntity());
            int sentOrder = 1;
            for (LeafString leafStringSent : paragraphLeaf.getListOfComponents()) {
                SentenceLeaf sentenceLeaf = (SentenceLeaf) leafStringSent;
                long sentenceId = sentenceRepository.create(new SentenceEntity(paragraphId, sentOrder++));
                int wordOrder = 1;
                for (LeafString leafStringWord : sentenceLeaf.getListOfComponents()) {
                    WordLeaf wordLeaf = (WordLeaf) leafStringWord;
                    wordRepository.create(new WordEntity(wordLeaf.getStrings(), sentenceId, wordOrder++));
                }
            }
            return paragraphId;
        } catch (CreatorException e) {
            throw new ProblemWithSaveException("paragraph cannot be saved. Some details", e);
        }
    }

    public ParagraphLeaf getParagraphModel(long id) throws ModelGettingException {
        try {
            if (paragraphRepository.getAllItems().stream().anyMatch(paragraphEntity -> paragraphEntity.getId() == id)) {
                ParagraphLeaf paragraphLeaf = new ParagraphLeaf();
                List<SentenceEntity> sentenceEntityList = sentenceRepository.getAllItemsBySourceId(id);
                for (SentenceEntity sentenceEntity : sentenceEntityList) {
                    SentenceLeaf sentenceLeaf = new SentenceLeaf();
                    List<WordEntity> wordEntityList = wordRepository.getAllItemsBySourceId(sentenceEntity.getId());
                    for (WordEntity wordEntity : wordEntityList) {
                        WordLeaf wordLeaf = new WordLeaf(wordEntity.getValue());
                        sentenceLeaf.addSubLeaf(wordLeaf);
                    }
                    paragraphLeaf.addSubLeaf(sentenceLeaf);
                }
                return paragraphLeaf;
            } else {
                throw new ModelGettingException("repository doesnt have text with this id");
            }
        } catch (IncorrectIdException e) {
            throw new ModelGettingException("cant get paragraph model from rep", e);
        }
    }

    public ParagraphEntity readById(long id) throws CantReadException {
        try {
            return paragraphRepository.read(id);
        } catch (IncorrectIdException e) {
            throw new CantReadException("cant read paragraph from rep", e);
        }
    }

    public List<ParagraphEntity> getAllBySourceId(long id) throws GetAllBySourceIdException {
        try {
            return paragraphRepository.getAllItemsBySourceId(id);
        } catch (IncorrectIdException e) {
            throw new GetAllBySourceIdException(e);
        }
    }

    public List<Long> getIdList() {
        return paragraphRepository.getListOfId();
    }

    public List<ParagraphEntity> getAllParagraphs() {
        return new LinkedList<>(paragraphRepository.getAllItems());
    }

    public boolean deleteParagraph(long id) {
        return paragraphRepository.delete(id);
    }

    public List<ParagraphEntity> sort(Comparator<ParagraphEntity> comparator) throws SortException {
        if (comparator == null) throw new SortException("cant sort paragraphs entities : comparator is null");
        List<ParagraphEntity> paragraphEntityList = new ArrayList<>(paragraphRepository.getAllItems());
        paragraphEntityList.sort(comparator);
        return paragraphEntityList;
    }

    public ParagraphLeaf getSortedParagraphModel(Comparator<LeafString> comparator, long id) throws SortException, ModelGettingException {
        if (comparator == null) throw new SortException("comparator is null");
        ParagraphLeaf paragraphLeaf = getParagraphModel(id);
        paragraphLeaf.getListOfComponents().sort(comparator);
        return paragraphLeaf;
    }
}
