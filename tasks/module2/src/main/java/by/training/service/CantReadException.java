package by.training.service;

public class CantReadException extends Exception {
    public Exception exception;

    public CantReadException() {

    }

    public CantReadException(String message) {
        super(message);
    }

    public CantReadException(String message, Exception exception) {
        super(message);
        this.exception = exception;
    }
}
