package by.training.repository;

import by.training.entity.TextEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class TextRepository implements Repository<TextEntity> {
    private AtomicLong generatorId = new AtomicLong(0);
    private List<TextEntity> listOfTexts;

    public TextRepository() {
        this.listOfTexts = new ArrayList<>();
    }

    @Override
    public long create(TextEntity item) throws CreatorException {
        if (item == null) throw new CreatorException("this text is null");
        if (listOfTexts.contains(item)) {
            return listOfTexts.get(listOfTexts.indexOf(item)).getId();
        } else {
            item.setId(generatorId.incrementAndGet());
            listOfTexts.add(item);
            return item.getId();
        }
    }

    @Override
    public boolean delete(long id) {
        if (listOfTexts.stream().anyMatch(textEntity -> textEntity.getId() == id)) {
            for (TextEntity textEntity : listOfTexts) {
                if (textEntity.getId() == id) {
                    listOfTexts.remove(textEntity);
                    return true;
                }
            }
        }
            return false;

    }

    @Override
    public TextEntity read(long id) throws IncorrectIdException {
        List<TextEntity> list = new ArrayList<>();
        listOfTexts.stream().filter(textEntity -> textEntity.getId() == id).forEach(list::add);
        if (list.isEmpty()) {
            throw new IncorrectIdException("This text id is not exist", id);
        } else {
            return list.get(0);
        }
    }

    @Override
    public List<Long> getListOfId() {
        List<Long> idList = new ArrayList<>();
        listOfTexts.stream().forEach(textEntity -> idList.add(textEntity.getId()));
        return idList;
    }


    @Override
    public List<TextEntity> getAllItems() {
        return new ArrayList<>(listOfTexts);
    }

    @Override
    public List<TextEntity> getAllItemsBySourceId(long id) {
        return getAllItems();
    }

    @Override
    public List<TextEntity> findBySpec(Specification<TextEntity> spec) {
        List<TextEntity> newListOfTexts = new ArrayList<>();
        listOfTexts.stream().filter(spec::isSatisfiedBy).forEach(newListOfTexts::add);
        return newListOfTexts;
    }


}
