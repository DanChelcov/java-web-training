package by.training.repository;

import java.util.List;

public interface Repository<T> {
    long create(T item) throws CreatorException;

    boolean delete(long id);

    T read(long id) throws IncorrectIdException;

    List<Long> getListOfId();

    List<T> getAllItems();

    List<T> getAllItemsBySourceId(long id) throws IncorrectIdException;

    List<T> findBySpec(Specification<T> spec);
}
