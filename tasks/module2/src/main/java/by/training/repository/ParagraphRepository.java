package by.training.repository;

import by.training.entity.ParagraphEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class ParagraphRepository implements Repository<ParagraphEntity> {
    private AtomicLong generatorId = new AtomicLong(0);
    private List<ParagraphEntity> listOfParagraphs;

    public ParagraphRepository() {
        this.listOfParagraphs = new ArrayList<>();
    }

    @Override
    public long create(ParagraphEntity item) throws CreatorException {
        if (item == null) throw new CreatorException("this paragraph is null");
        if (listOfParagraphs.contains(item)) {
            return listOfParagraphs.get(listOfParagraphs.indexOf(item)).getId();
        } else {
            item.setId(generatorId.incrementAndGet());
            listOfParagraphs.add(item);
            return item.getId();
        }
    }

    @Override
    public boolean delete(long id) {
        if (listOfParagraphs.stream().anyMatch(textEntity -> textEntity.getId() == id)) {
            for (ParagraphEntity textEntity : listOfParagraphs) {
                if (textEntity.getId() == id) {
                    listOfParagraphs.remove(textEntity);
                    return true;
                }
            }
        }
        return false;

    }

    @Override
    public ParagraphEntity read(long id) throws IncorrectIdException {
        List<ParagraphEntity> list = new ArrayList<>();
        listOfParagraphs.stream().filter(paragraphEntity -> paragraphEntity.getId() == id).forEach(list::add);
        if (list.isEmpty()) {
            throw new IncorrectIdException("This id is not exist", id);
        } else {
            return list.get(0);
        }
    }

    @Override
    public List<Long> getListOfId() {
        List<Long> idList = new ArrayList<>();
        listOfParagraphs.stream().forEach(paragraphEntity -> idList.add(paragraphEntity.getId()));
        return idList;
    }

    @Override
    public List<ParagraphEntity> getAllItems() {
        return new ArrayList<>(listOfParagraphs);
    }

    @Override
    public List<ParagraphEntity> getAllItemsBySourceId(long id) throws IncorrectIdException {
        List<ParagraphEntity> paragraphEntityList = new ArrayList<>();
        listOfParagraphs.stream().
                filter(paragraphEntity -> paragraphEntity.getTextId() == id).
                forEach(paragraphEntityList::add);
        if(paragraphEntityList.isEmpty()) throw new IncorrectIdException("There are no paragraphs by such id", id);
        return paragraphEntityList;
    }

    @Override
    public List<ParagraphEntity> findBySpec(Specification<ParagraphEntity> spec) {
        List<ParagraphEntity> newListOfParagraphs = new ArrayList<>();
        listOfParagraphs.stream().filter(spec::isSatisfiedBy).forEach(newListOfParagraphs::add);
        return newListOfParagraphs;
    }
}
