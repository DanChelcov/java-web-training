package by.training.repository;

import by.training.entity.SentenceEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class SentenceRepository implements Repository<SentenceEntity> {
    private AtomicLong generatorId = new AtomicLong(0);
    private List<SentenceEntity> listOfSentences;

    public SentenceRepository() {
        this.listOfSentences = new ArrayList<>();
    }

    @Override
    public long create(SentenceEntity item) throws CreatorException {
        if (item == null) throw new CreatorException("this sentence is null");
        if (listOfSentences.contains(item)) {
            return listOfSentences.get(listOfSentences.indexOf(item)).getId();
        } else {
            item.setId(generatorId.incrementAndGet());
            listOfSentences.add(item);
            return item.getId();
        }
    }

    @Override
    public boolean delete(long id) {
        if (listOfSentences.stream().anyMatch(textEntity -> textEntity.getId() == id)) {
            for (SentenceEntity textEntity : listOfSentences) {
                if (textEntity.getId() == id) {
                    listOfSentences.remove(textEntity);
                    return true;
                }
            }
        }
        return false;

    }

    @Override
    public SentenceEntity read(long id) throws IncorrectIdException {
        List<SentenceEntity> list = new ArrayList<>();
        listOfSentences.stream().filter(paragraphEntity -> paragraphEntity.getId() == id).forEach(list::add);
        if (list.isEmpty()) {
            throw new IncorrectIdException("This sentences id is not exist", id);
        } else {
            return list.get(0);
        }
    }

    @Override
    public List<Long> getListOfId() {
        List<Long> idList = new ArrayList<>();
        listOfSentences.stream().forEach(sentenceEntity -> idList.add(sentenceEntity.getId()));
        return idList;
    }

    @Override
    public List<SentenceEntity> getAllItems() {
        return new ArrayList<>(listOfSentences);
    }

    @Override
    public List<SentenceEntity> getAllItemsBySourceId(long id) throws IncorrectIdException {
        List<SentenceEntity> sentenceEntityList = new ArrayList<>();
        listOfSentences.stream().
                filter(paragraphEntity -> paragraphEntity.getParagraphId() == id).
                forEach(sentenceEntityList::add);
        if(sentenceEntityList.isEmpty()) throw new IncorrectIdException("There are no sentences by such id", id);
        return sentenceEntityList;
    }

    @Override
    public List<SentenceEntity> findBySpec(Specification<SentenceEntity> spec) {
        List<SentenceEntity> newListOfSentences = new ArrayList<>();
        listOfSentences.stream().filter(spec::isSatisfiedBy).forEach(newListOfSentences::add);
        return newListOfSentences;
    }
}
