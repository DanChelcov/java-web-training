package by.training.repository;

public class CreatorException extends Exception {
    public Exception exception;

    public CreatorException() {
    }

    public CreatorException(String message) {
        super(message);
    }
}
