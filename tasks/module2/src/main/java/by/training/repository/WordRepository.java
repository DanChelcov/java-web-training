package by.training.repository;

import by.training.entity.WordEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class WordRepository implements Repository<WordEntity> {
    private AtomicLong generatorId = new AtomicLong(0);
    private List<WordEntity> listOfWords;

    public WordRepository() {
        this.listOfWords = new ArrayList<>();
    }


    @Override
    public long create(WordEntity item) throws CreatorException {
        if (item == null) throw new CreatorException("this word is null");
        item.setId(generatorId.incrementAndGet());
        listOfWords.add(item);
        return item.getId();

    }

    @Override
    public boolean delete(long id) {
        if (listOfWords.stream().anyMatch(textEntity -> textEntity.getId() == id)) {
            for (WordEntity textEntity : listOfWords) {
                if (textEntity.getId() == id) {
                    listOfWords.remove(textEntity);
                    return true;
                }
            }
        }
        return false;

    }

    @Override
    public WordEntity read(long id) throws IncorrectIdException {
        List<WordEntity> list = new ArrayList<>();
        listOfWords.stream().filter(paragraphEntity -> paragraphEntity.getId() == id).forEach(list::add);
        if (list.isEmpty()) {
            throw new IncorrectIdException("This word id is not exist", id);
        } else {
            return list.get(0);
        }
    }

    @Override
    public List<Long> getListOfId() {
        List<Long> idList = new ArrayList<>();
        listOfWords.stream().forEach(wordEntity -> idList.add(wordEntity.getId()));
        return idList;
    }

    @Override
    public List<WordEntity> getAllItems() {
        return new ArrayList<>(listOfWords);
    }

    @Override
    public List<WordEntity> getAllItemsBySourceId(long id) throws IncorrectIdException {
        List<WordEntity> wordEntityList = new ArrayList<>();
        listOfWords.stream().
                filter(paragraphEntity -> paragraphEntity.getSentenceId() == id).
                forEach(wordEntityList::add);
        if(wordEntityList.isEmpty()) throw new IncorrectIdException("There are no wordsz     by such id", id);

        return wordEntityList;
    }

    @Override
    public List<WordEntity> findBySpec(Specification<WordEntity> spec) {
        List<WordEntity> newListOfWords = new ArrayList<>();
        listOfWords.stream().filter(spec::isSatisfiedBy).forEach(newListOfWords::add);
        return newListOfWords;
    }
}
