package by.training.controller;

import by.training.comparator.ParagraphSortSentencesLengthComparator;
import by.training.entity.ParagraphEntity;
import by.training.model.LeafString;
import by.training.model.ParagraphLeaf;
import by.training.model.TextLeaf;
import by.training.parser.FullTextParser;
import by.training.service.*;
import by.training.validator.FileValidator;
import by.training.validator.ValidationResult;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;


public class TextController {
    private static final Logger LOGGER = LogManager.getLogger(TextController.class);

    private final TextService textService;
    private final ParagraphService paragraphService;
    private final SentenceService sentenceService;
    private final WordService wordService;

    private FileValidator fileValidator;
    private FullTextParser fullTextParser;
    private Reader reader;

    public TextController(TextService textService, ParagraphService paragraphService,
                          SentenceService sentenceService, WordService wordService) {
        this.textService = textService;
        this.paragraphService = paragraphService;
        this.sentenceService = sentenceService;
        this.wordService = wordService;
        fileValidator = new FileValidator();
        fullTextParser = new FullTextParser();
        reader = new Reader();
    }

    public long readInfoFromFile(String parse) {
        ValidationResult validationResult = fileValidator.getValidationResult(parse);
        if (validationResult.isValid()) {
            try {
                String text = reader.readAllText(parse);
                TextLeaf textLeaf = fullTextParser.getTextLeaf(text);
                long id = textService.save(textLeaf);
                LOGGER.info("text has be successfully added to rep");
                return id;
            } catch (IOException e) {
                LOGGER.error("problem with file", e);
            } catch (ProblemWithSaveException e) {
                LOGGER.error("problem with read info from file", e);
            }
        } else {
            LOGGER.error("problem with file : " + validationResult.getAllVales());
        }
        return -1;
    }

    public TextLeaf getTextWithSortedParagraphsBySentencesLength(long id, boolean reverse) {
        TextLeaf textLeaf = new TextLeaf();
        ParagraphSortSentencesLengthComparator comparator = new ParagraphSortSentencesLengthComparator(reverse);
        try {
            List<ParagraphEntity> list = paragraphService.getAllBySourceId(id);
            List<LeafString> paragraphLeafList = new LinkedList<>();
            for (ParagraphEntity paragraphEntity : list) {
                paragraphLeafList.
                        add(paragraphService.getSortedParagraphModel(comparator, paragraphEntity.getId()));
            }
            textLeaf.addListOfSubLeafs(paragraphLeafList);
        } catch (ModelGettingException e) {
            LOGGER.error("cant sort paragraphs in text by sentence", e);
        } catch (GetAllBySourceIdException e) {
            LOGGER.error("cant sort paragraphs in text by sentence", e);
        } catch (SortException e) {
            LOGGER.error("cant sort paragraphs in text by sentence", e);
        }
        return textLeaf;


    }

    public ParagraphLeaf getSortedParagraph(long id, boolean reverse) {
        ParagraphSortSentencesLengthComparator comparator = new ParagraphSortSentencesLengthComparator(reverse);
        ParagraphLeaf paragraphLeaf = new ParagraphLeaf();
        try {
            paragraphLeaf = paragraphService.getSortedParagraphModel(comparator, id);
        } catch (ModelGettingException e) {
            LOGGER.error("cant sort paragraphs in text by sentence", e);
        } catch (SortException e) {
            LOGGER.error("cant sort paragraphs in text by sentence", e);
        }
        return paragraphLeaf;
    }

}
