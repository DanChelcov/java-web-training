package by.training.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Reader {
    public String readAllText(String filePath) throws IOException {
        List<String> lineList = Files.readAllLines(Paths.get(filePath));
        return String.join("\n", lineList);
    }


}
