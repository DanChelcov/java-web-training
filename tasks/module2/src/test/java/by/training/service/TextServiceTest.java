package by.training.service;

import by.training.entity.ParagraphEntity;
import by.training.entity.SentenceEntity;
import by.training.entity.TextEntity;
import by.training.entity.WordEntity;
import by.training.model.ParagraphLeaf;
import by.training.model.SentenceLeaf;
import by.training.model.TextLeaf;
import by.training.model.WordLeaf;
import by.training.repository.*;
import org.junit.Before;
import org.junit.Test;

import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class TextServiceTest {
    TextService textService;
    TextRepository textRepository;
    ParagraphRepository paragraphRepository;
    SentenceRepository sentenceRepository;
    WordRepository wordRepository;
    WordLeaf wordLeaf = new WordLeaf("ggdg");
    SentenceLeaf sentenceLeaf = new SentenceLeaf();
    ParagraphLeaf paragraphLeaf = new ParagraphLeaf();
    TextLeaf textLeaf = new TextLeaf();
    TextEntity textEntity;
    ParagraphEntity paragraphEntity;
    SentenceEntity sentenceEntity;
    WordEntity wordEntity;


    @Before
    public void init() throws CreatorException {
        textRepository = new TextRepository();
        paragraphRepository = new ParagraphRepository();
        sentenceRepository = new SentenceRepository();
        wordRepository = new WordRepository();
        textService = new TextService(textRepository, paragraphRepository, sentenceRepository, wordRepository);
        sentenceLeaf.addSubLeaf(wordLeaf);
        paragraphLeaf.addSubLeaf(sentenceLeaf);
        textLeaf.addSubLeaf(paragraphLeaf);
        textEntity = new TextEntity();
        textRepository.create(textEntity);
        paragraphEntity = new ParagraphEntity(textEntity.getId());
        paragraphRepository.create(paragraphEntity);
        sentenceEntity = new SentenceEntity(paragraphEntity.getId());
        sentenceRepository.create(sentenceEntity);
        wordEntity = new WordEntity("ggdg", sentenceEntity.getId());
    }

    @Test
    public void save() throws ProblemWithSaveException {
        textService.save(textLeaf);
        assertTrue(!textRepository.getAllItems().isEmpty());
        assertTrue(!paragraphRepository.getAllItems().isEmpty());
        assertTrue(!sentenceRepository.getAllItems().isEmpty());
        assertTrue(!wordRepository.getAllItems().isEmpty());
        assertEquals("ggdg", wordRepository.getAllItems().get(0).getValue().trim());
    }

    @Test
    public void getTextModel() throws ModelGettingException, CreatorException {
        textRepository.create(textEntity);
        paragraphRepository.create(paragraphEntity);
        sentenceRepository.create(sentenceEntity);
        wordRepository.create(wordEntity);
        TextLeaf textLeaf = textService.getTextModel(1);
        assertTrue(textLeaf != null);
        assertTrue(textLeaf.getNumberOfElements() != 0);
        assertEquals("ggdg", textLeaf.getStrings().trim());


    }

    @Test
    public void readById() throws CreatorException, CantReadException {
        textRepository.create(textEntity);
        assertEquals(textEntity, textService.readById(1));
    }

    @Test
    public void getIdList() throws CreatorException {
        textRepository.create(new TextEntity());
        textRepository.create(new TextEntity());
        textRepository.create(new TextEntity());
        assertEquals(4, textService.getIdList().size());
    }


    @Test
    public void deleteText() {
        int i = textRepository.getAllItems().size();
        textService.deleteText(1);
        assertFalse(textRepository.getListOfId().size() == i);
    }


}