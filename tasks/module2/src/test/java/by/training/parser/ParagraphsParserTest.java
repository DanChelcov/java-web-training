package by.training.parser;

import by.training.model.TextLeaf;
import org.junit.Test;

import static org.junit.Assert.*;

public class ParagraphsParserTest {
    String string = "\tIt has survived not only five centuries, but also the leap into electronic " +
            "typesetting, remaining essentially unchanged. It was popularised in the with the " +
            "release of Letraset sheets containing Lorem Ipsum passages, and more recently with " +
            "desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n" +
            "\tIt is a long established fact that a reader will be distracted by the readable" +
            "content of a page when looking at its layout. The point of using Ipsum is that it has a" +
            "more-or-less normal distribution of letters, as opposed to using 'Content here, content" +
            "here', making it look like readable English.\n";
    WordsParser wordsParser = new WordsParser();
    SentencesParser sentencesParser = new SentencesParser(wordsParser);
    ParagraphsParser paragraphsParser = new ParagraphsParser(sentencesParser);

    @Test
    public void checkChainOfParses() {
        assertTrue(!sentencesParser.processString(string).isEmpty());
        assertTrue(!paragraphsParser.processString(string).isEmpty());
        assertTrue(!wordsParser.processString(string).isEmpty());
        assertEquals(4, sentencesParser.processString(string).size());
        assertEquals(2, paragraphsParser.processString(string).size());
        assertEquals(97, wordsParser.processString(string).size());
TextLeaf textLeaf = new TextLeaf();
textLeaf.addListOfSubLeafs(paragraphsParser.processString(string));
        assertEquals(string, textLeaf.getStrings());

    }
}
