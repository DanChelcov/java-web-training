package by.training.controller;

import by.training.model.ParagraphLeaf;
import by.training.model.TextLeaf;
import by.training.repository.*;
import by.training.service.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

public class TextControllerTest {
    TextRepository textRepository;
    ParagraphRepository paragraphRepository;
    SentenceRepository sentenceRepository;
    WordRepository wordRepository;
    TextService textService;
    ParagraphService paragraphService;
    SentenceService sentenceService;
    WordService wordService;
    TextController textController;
    String pathToFile;

    @Before
    public void init() {
        textRepository = new TextRepository();
        paragraphRepository = new ParagraphRepository();
        sentenceRepository = new SentenceRepository();
        wordRepository = new WordRepository();
        textService = new TextService(textRepository, paragraphRepository, sentenceRepository, wordRepository);
        paragraphService = new ParagraphService(paragraphRepository, sentenceRepository, wordRepository);
        sentenceService = new SentenceService(sentenceRepository, wordRepository);
        wordService = new WordService(wordRepository);
        textController = new TextController(textService, paragraphService, sentenceService, wordService);
        pathToFile = new File(this.getClass().getResource("/test.txt").getFile()).getAbsolutePath();
    }

    @Test
    public void readInfoFromFile() throws ModelGettingException {
        long textId = textController.readInfoFromFile(pathToFile);
        Assert.assertEquals(1, textId);
        Assert.assertEquals(1, textRepository.getAllItems().size());
        Assert.assertEquals(4, paragraphRepository.getAllItems().size());
        Assert.assertEquals(6, sentenceRepository.getAllItems().size());
        Assert.assertEquals(119, wordRepository.getAllItems().size());
    }

    @Test
    public void IndividualEx() throws IncorrectIdException {
        textController.readInfoFromFile(pathToFile);
        Assert.assertFalse(textRepository.getAllItems().isEmpty());
        TextLeaf textLeaf = textController.getTextWithSortedParagraphsBySentencesLength(1, false);
        System.out.println(textLeaf.getStrings());
    }

    @Test
    public void IndividualEx2() throws IncorrectIdException {
        textController.readInfoFromFile(pathToFile);
        Assert.assertFalse(paragraphRepository.getAllItems().isEmpty());
        ParagraphLeaf paragraphLeaf = textController.getSortedParagraph(1, true);
        ParagraphLeaf paragraphLeaf1 = textController.getSortedParagraph(1, false);
        System.out.println(paragraphLeaf.getStrings());
        System.out.println(paragraphLeaf1.getStrings());
    }
}